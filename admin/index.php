<!DOCTYPE html>
<html lang="en">
<body>
<?php
include ('partial/head.php');
?>
<div id="preloader"></div>
<div id="wrapper">
<?php
include ('partial/header.php');
if (isset($_GET['dashboard'])) {
	include ('content/dashboard.php');
} else if (isset($_GET['manage-accounts'])) {
	include ('content/manage-accounts.php');
} else if (isset($_GET['profile'])) {
	include ('content/profile.php');
} else if (isset($_GET['user-list'])) {
	include ('content/user-list.php');
} else if (isset($_GET['add-account'])) {
	include ('content/add-account.php');
} else if (isset($_GET['edit-account'])) {
	include ('content/edit-account.php');
} else if (isset($_GET['detail-account'])) {
	include ('content/detail-account.php');
} else if (isset($_GET['event'])) {
	include ('content/event.php');
} else if (isset($_GET['page-event'])) {
	include ('content/page-event.php');
} else if (isset($_GET['page-policy'])) {
	include ('content/page-policy.php');
} else if (isset($_GET['inventory'])) {
	include ('content/inventory.php');
} else if (isset($_GET['order-from'])) {
	include ('content/order-from.php');
} else if (isset($_GET['policy'])) {
	include ('content/policy.php');
} else {
	include ('content/dashboard.php');
}
include ('partial/footer.php');
?>
</div>
</body>
</html>