	//General Variables
	var attack_root = null ;
	var summary_mode = true ;
	var selectedNodeData = null;
	var all_nodes = null;
	var svgGroup = null ;
	var start_sid = null ;

	var myTabs = Object.freeze({Proc: 0, ProcSec: 1, FileOps: 2,  NetOp: 7, RegOps: 8, SpecialEvents: 10,  InjectOps: 13, Damage: 11, Reputation: 12});
	var mySelectedTab = myTabs.Proc ;
	
	var ProcPanelEnum = Object.freeze({MINIMIZED: 0, MAXIMIZED: 1, REGULAR: 2});
	var procPanelState = ProcPanelEnum.MINIMIZED ;
	
	//height width Variables
	var viewerHeight = null ; 
	
	//Node image and text Variables
	var radius_default = 24 ;
	var radius_exclude_transparent = 20 ;
	var selector_below = 5 ;
	
	var text_left_of_node_x = -30 ;
	var text_right_of_node_x = 30 ;
	var text_below_node_y = 12 ;	
	var text_padding = 10 ;
	
	//DataTables Variables
	var url_datatable = null ;
	var iana_datatabl = null ;
	var filecreate_datatable = null ;
	var filerename_datatable = null ;
	var filedelete_datatable = null ;
	var filewrite_datatable = null ;
	var fileread_datatable = null ;
	var regwrite_datatable = null ;
	var regread_datatable = null ;
	var injecthook_datatable = null ;
	var damage_datatable = null ;
	
	//Other Variables
	var regularProcPanelHeight = 300 ; //If css changes, this needs to change.
	var minimizedProcPanelHeight = 53 ; //If css changes, this needs to change.

	//Selection divs for radio buttons in process info window
	var selected_file_div = null ;
	var selected_reg_div = null ;
	var selected_net_div = null ;
	
	//Operation Divs
	var div_file_create = null ;
	var div_file_rename = null ;
	var div_file_delete = null ;
	var div_file_write = null ;
	var div_file_read = null ;
	var div_net_http = null ;
	var div_net_other = null ;
	var div_reg_write = null ;
	var div_reg_read = null ;
	
	var div_text_width = null ;
	
	//Buttons
	var sum_button = null ;
	var comp_button = null ;

	// A recursive helper function for performing some setup by walking through all nodes
	function visit(parent, visitFn, childrenFn) {
		if (!parent) return null;

		if (parent.real_root == 1)
		{
			attack_root = parent ;
		}
		visitFn(parent);

		var children = childrenFn(parent);
		if (children) {
			var count = children.length;
			for (var i = 0; i < count; i++) {
				visit(children[i], visitFn, childrenFn);
			}
		}
		
		return attack_root ;
	}

	// Toggle children function
	function toggleChildren(d) {
		if (d.children) {
			d._children = d.children;
			d.children = null;
		} else if (d._children) {
			d.children = d._children;
			d._children = null;
		}
		return d;
	}

	// Helper functions for collapsing and expanding nodes.
	function collapse(d) {
		if (d.children) {
			d._children = d.children;
			d._children.forEach(collapse);
			d.children = null;
		}
	}

	function expand(d) {
		if (d._children) {
			d.children = d._children;
			d.children.forEach(expand);
			d._children = null;
		}
	}
	
	function getTextWidth2(text, font_size)
	{
			div_text_width.innerHTML = text ;
			div_text_width.style.fontSize = font_size ;
			//var height = (div_text_width.clientHeight + 1) ;
			return (div_text_width.clientWidth + 1) ;
	}
	
	/**
	 * Uses canvas.measureText to compute and return the width of the given text of given font in pixels.
	 * 
	 * @param {String} text The text to be rendered.
	 * @param {String} font The css font descriptor that text is to be rendered with (e.g. "bold 14px verdana").
	 */
	function getTextWidth(text, font) {
		// re-use canvas object for better performance
		var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));
		var context = canvas.getContext("2d");
		context.font = font;
		var metrics = context.measureText(text);
		return metrics.width;
	};
	
	// Used to create an overlay effect when the legend is displayed.
	function legendOverlay() {
		el = document.getElementById("legend-overlay") ;
		if (el.style.visibility == "visible")
			el.style.visibility = "hidden" ;
		else
			el.style.visibility = "visible";
	}

	// Function to center node when clicked/dropped so node doesn't get lost when collapsing/moving with large amount of children.
	function centerNode(source, zoomListener, effectiveWidth, effectiveHeight, duration) {
		scale = zoomListener.scale();
		x = -source.y0;
		y = -source.x0;
		x = x * scale + effectiveWidth / 2;
		y = y * scale + effectiveHeight / 2;
		d3.select('g').transition()
			.duration(duration)
			.attr("transform", "translate(" + x + "," + y + ")scale(" + scale + ")");
		zoomListener.scale(scale);
		zoomListener.translate([x, y]);
	}

	// sort the tree according to the time
	function sortTree(tree_or_nodes) {
		tree_or_nodes.sort(function(a, b) {
			return b.time_int < a.time_int ? 1 : -1 ;
		});
	}

	function showProcessInfo(d) {
		$('#tooltip').stop(true, true) ;
		$('#tooltip').text(d.path + "\n" + d.tooltip).css({left: d3.event.pageX - $('#tooltip').width()/2, top: d3.event.pageY - 3*$('#tooltip').height()}).fadeIn('fast').fadeOut(9000);
	}
	
	var procJS_Set = new Set() ;
	function dynamicAddProcInfoJS(d)
	{
		//removeProcScript() ;
		var lsFileName = "json\\procs\\json_" + d.pid + "_" + d.time_int + ".js" ;
		if (procJS_Set.has(lsFileName))
		{
			showNodeText(d, true) ;
			return ;
		}

		procJS_Set.add(lsFileName) ;
		loadProcScript(lsFileName, d, true) ;	
	}

	function se_handle(seData, se_tab_prefix) {
		var se_div = $('#proc_suspicious_events_div')[0] ;
		while (se_div.hasChildNodes()) {
			se_div.removeChild(se_div.lastChild);
		}

		var lncount = 0 ;
		for(var j in seData)
		{
			var HR = document.createElement('hr') ;
			HR.style.border = "none" ;
			HR.style.borderTop = "dotted 1px" ;	
			
			var category = document.createTextNode(seData[j].se_category) ;
			var category_div = document.createElement('div') ;
			var category_details = document.createElement('details') ;
			var summary = document.createElement('summary') ;
			category_div.style.minHeight = "40px" ;

			var score_image = document.createElement("img");
			score_image.src = "images\\" + seData[j].se_category_score_image;
			score_image.style.cssFloat = "right" ;
			score_image.style.paddingTop = "15px" ;
							
			summary.appendChild(category) ;
			summary.appendChild(score_image) ;

			category_details.appendChild(summary); 
			category_details.appendChild(document.createElement('br')) ;
			category_details.appendChild(document.createTextNode(seData[j].se_category_desc)) ;
			category_details.appendChild(document.createElement('br')) ;
			category_details.appendChild(document.createElement('br')) ;
			
			category_div.appendChild(HR) ;
			category_div.appendChild(category_details) ;
		
			var tblContainer = document.createElement('div');
			tblContainer.className = "table_div" ;

			var seTable = document.createElement('table') ;
			seTable.className = "stripe hover cell-border" ;
			seTable.id= "p_tbl_susp_" + j ;
			
			var seThead = document.createElement('thead') ;
			var seTbody = document.createElement('tbody') ;
			var seRow = document.createElement('tr') ;
			var seTd1 = document.createElement('td') ;
			var seTd2 = document.createElement('td') ;
			var col1 = document.createElement('col') ;
			var col2 = document.createElement('col') ;
			col1.width = "80%" ;
			col2.width = "20%" ;
			
			seTable.appendChild(col1) ;
			seTable.appendChild(col2) ;			

			var header1 = document.createTextNode("Description") ;
			var header2 = document.createTextNode("Time") ;
			
			seTd1.appendChild(header1) ;
			seTd2.appendChild(header2) ;

			seRow.appendChild(seTd1) ;
			seRow.appendChild(seTd2) ;
			
			seThead.appendChild(seRow) ;
			
			seTable.appendChild(col1) ;
			seTable.appendChild(col2) ;
			seTable.appendChild(seThead) ;
			seTable.appendChild(seTbody) ;
			tblContainer.appendChild(seTable) ;
		
			category_details.appendChild(tblContainer) ;
			category_div.appendChild(category_details) ;	
			se_div.appendChild(category_div) ;
			
			var suspicious_data = [];	
			var suspicious_datatable = $("#" + seTable.id).DataTable( {
				"iDisplayLength": 100,
				"paging": false,
				"searching": false,
				"info": false,
				"columnDefs": [ { className: "RightTextCol", "targets": [ -1 ] },
								{"targets": -1, "data": 1, "render": function ( data, type, full, meta ) {
									return type === 'display' || type === 'filter' ? getDateTimeString(data) :  data;
								}} ]
			} );
			
			suspicious_data.length = 0;
			suspicious_datatable.clear().draw() ;			
			
			var seEvents = seData[j].events ;
			for( var i in seEvents)
			{
				suspicious_data.push([seEvents[i].se_description, seEvents[i].time_int]);
				++lncount ;
			}
			
			suspicious_datatable.rows.add(suspicious_data).draw() ;
		}			
		
		$('#p_tab11').text(se_tab_prefix + "(" + (lncount) +")") ;
		if (lncount > 0)
			setDetailsSummaryShim();
	}
		
	function createDataTables()
	{
		url_datatable = $('#url_table').DataTable( {
			"iDisplayLength": 100,
			 "columnDefs": [ { className: "RightTextCol", "targets": [ -1, -2, -3, -4, -5] },
								{"targets": -1, "data": 8, "render": function ( data, type, full, meta ) {
									return type === 'display' || type === 'filter' ? getDateTimeString(data) :  data;
								}} ]
		} ); 		

		iana_datatable = $('#iana_table').DataTable( {
			"iDisplayLength": 100,
			 "columnDefs": [ { className: "RightTextCol", "targets": [ -1, -2, -3, -4, -5 ] },
								{"targets": -1, "data": 7, "render": function ( data, type, full, meta ) {
									return type === 'display' || type === 'filter' ? getDateTimeString(data) :  data;
								}} ]
		} ); 		

		filecreate_datatable = $('#filecreate_table').DataTable( {
			"iDisplayLength": 100,
			 "columnDefs": [ { className: "RightTextCol", "targets": [ -1, -2 ] },
								{"targets": -1, "data": 3, "render": function ( data, type, full, meta ) {
									return type === 'display' || type === 'filter' ? getDateTimeString(data) :  data;
								}} ]
		} );

		filerename_datatable = $('#filerename_table').DataTable( {
			"iDisplayLength": 100,
			 "columnDefs": [ { className: "RightTextCol", "targets": [ -1, -2 ] },
								{"targets": -1, "data": 5, "render": function ( data, type, full, meta ) {
									return type === 'display' || type === 'filter' ? getDateTimeString(data) :  data;
								}} ]
		} );

		filedelete_datatable = $('#filedelete_table').DataTable( {
			"iDisplayLength": 100,
			 "columnDefs": [ { className: "RightTextCol", "targets": [ -1, -2 ] },
								{"targets": -1, "data": 3, "render": function ( data, type, full, meta ) {
									return type === 'display' || type === 'filter' ? getDateTimeString(data) :  data;
								}} ]
		} );

		filewrite_datatable = $('#filewrite_table').DataTable( {
			"iDisplayLength": 100,
			 "columnDefs": [ { className: "RightTextCol", "targets": [ -1, -2 ] },
								{"targets": -1, "data": 3, "render": function ( data, type, full, meta ) {
									return type === 'display' || type === 'filter' ? getDateTimeString(data) :  data;
								}} ]
		} );

		fileread_datatable = $('#fileread_table').DataTable( {
			"iDisplayLength": 100,
			 "columnDefs": [ { className: "RightTextCol", "targets": [ -1, -2 ] },
								{"targets": -1, "data": 3, "render": function ( data, type, full, meta ) {
									return type === 'display' || type === 'filter' ? getDateTimeString(data) :  data;
								}} ]
		} );

		regwrite_datatable = $('#regwrite_table').DataTable( {
			"iDisplayLength": 100,
			 "columnDefs": [ { className: "RightTextCol", "targets": [ -1 ] } ,
								{"targets": -1, "data": 5, "render": function ( data, type, full, meta ) {
									return type === 'display' || type === 'filter' ? getDateTimeString(data) :  data;
								}} ]
		} );

		regread_datatable = $('#regread_table').DataTable( {
			"iDisplayLength": 100,
			 "columnDefs": [	{ className: "RightTextCol", "targets": [ -1 ] },
								{"targets": -1, "data": 4, "render": function ( data, type, full, meta ) { 
									return type === 'display' || type === 'filter' ? getDateTimeString(data) :  data;
								}} ]	
		} );

		injecthook_datatable = $('#injecthook_table').DataTable( {
			"iDisplayLength": 100,
			 "columnDefs": [ { className: "RightTextCol", "targets": [ -1 ] },
								{"targets": -1, "data": 4, "render": function ( data, type, full, meta ) {
									return type === 'display' || type === 'filter' ? getDateTimeString(data) :  data;
								}} ]
		} );
		
		damage_datatable = $('#damageevents_table').DataTable( {
			"iDisplayLength": 100,
			 "columnDefs": [ { className: "RightTextCol", "targets": [ -1 ] },
								{"targets": -1, "data": 2, "render": function ( data, type, full, meta ) {
									return type === 'display' || type === 'filter' ? getDateTimeString(data) :  data;
								}} ]
		} );
	}
	
	function showNodeText(d, draw_creation_links) {
		selectedNodeData = d;
		var procForm = document.forms['process_details_form'];
		var procsecForm = document.forms['process_security_form'];
		var repForm = document.forms['process_reputation_form'];

		var filecreateLbl = $('#lbl_f_create');
		var filerenameLbl = $('#lbl_f_rename');
		var filedeleteLbl = $('#lbl_f_delete');
		var filewriteLbl = $('#lbl_f_write');
		var filereadLbl = $('#lbl_f_read');
		var regwriteLbl = $('#lbl_r_write');
		var regreadLbl = $('#lbl_r_read');
		var nethttpLbl = $('#lbl_n_http');
		var netianaLbl = $('#lbl_n_other');

		var seTable = $('#specialevents_table')  ;
		var dmgTable = $('#damageevents_table')  ;

		var proc_tab = $('#p_tab1') ;	
		var procsec_tab = $('#p_tab2') ;
		var fileop_tab = $('#p_tab3') ;	
		var netop_tab = $('#p_tab8') ;	
		var regop_tab = $('#p_tab9') ;
		var se_tab = $('#p_tab11') ;
		var dmg_tab = $('#p_tab12') ;
		var rep_tab = $('#p_tab13') ;		
		var inj_tab = $('#p_tab14') ;		

		var selection_list = $('#ViewSelector') ;

		var create_radio_prefix = "Creates " ;	
		var rename_radio_prefix = "Renames " ;	
		var delete_radio_prefix = "Deletes " ;	
		var write_radio_prefix = "Writes " ;	
		var read_radio_prefix = "Reads " ;	

		var fileop_tab_prefix = "File Ops " ;
		var netop_tab_prefix = "Network Ops " ;	
		var regop_tab_prefix = "Registry Ops " ;
		var se_tab_prefix = "Suspicious Events " ;
		var dmg_tab_prefix = "Damage " ;
		var inj_tab_prefix = "Injection/Hook Ops " ;

		var url_data = [];
		var iana_data = [];	
		var filecreate_data = [];	
		var filerename_data = [];	
		var filedelete_data = [];	
		var filewrite_data = [];	
		var fileread_data = [];	
		var regwrite_data = [];	
		var regread_data = [];	
		var injecthook_data = [];	
		var damage_data = [];	
		
		
		procForm.reset() ;
		procsecForm.reset() ;
		
		$(proc_tab).text(d.name) ;//set proc_tab name to d.name
		procForm.elements["proc_name_field"].value = d.name ;
		procForm.elements["proc_path_field"].value = d.path ;
		procForm.elements["proc_args_field"].value = d.args ;
		procForm.elements["proc_pid_field"].value = d.pid ;
		procForm.elements["proc_create-time_field"].value = getDateTimeString(d.time_int) ;
		procForm.elements["proc_close-time_field"].value = getDateTimeString(d.stop_time_int) ;
		procForm.elements["proc_duration_field"].value = getDuration(d.time_int, d.stop_time_int) ;
		procForm.elements["proc_created_by_field"].value = d.created ;
		procForm.elements["created_pid_field"].value = d.created_pid ;
		procForm.elements["proc_execution_calls_field"].value = d.execution_calls ;
		
		procsecForm.elements["proc_md5_field"].value = d.md5 ;
		procsecForm.elements["proc_sid_field"].value = d.sid ;
		procsecForm.elements["proc_sigcompany_field"].value = d.sig_company ;				
		procsecForm.elements["proc_username_field"].value = d.username ;
		procsecForm.elements["proc_msdnID_field"].value = d.sid_msdn ;
		procsecForm.elements["proc_msdnDesc_field"].value = d.sid_desc ;
		
		repForm.elements["rep_decision_field"].value = d.rep_decision ;
		repForm.elements["rep_threat_field"].value = d.rep_threat ;
		repForm.elements["rep_trust_field"].value = d.rep_trust ;
		if (d.rep_scanners == "" || d.rep_detections == "")
		{
			repForm.elements["rep_detections_field"].value = "Not Available" ;			
		}
		else
		{
			repForm.elements["rep_detections_field"].value = "Virus-Total detections: " + d.rep_detections + "/" + d.rep_scanners + ", detected malicious by " + d.rep_detection_ratio + " of AV scanners." ;
		}
		
		//repForm.elements["rep_scanners_field"].value = d.rep_scanners ;
		//repForm.elements["rep_detection_pct_field"].value = d.rep_detection_ratio ;
		repForm.elements["rep_file_name_field"].value = d.rep_file_name ;
		repForm.elements["rep_file_type_field"].value = d.rep_file_type ;
		repForm.elements["rep_file_size_field"].value = d.rep_file_size ;
		repForm.elements["rep_file_version_field"].value = d.rep_file_version ;
		repForm.elements["rep_company_field"].value = d.rep_company ;
		repForm.elements["rep_publisher_field"].value = d.rep_publisher ;

		$(dmgTable.rows).slice(1).remove() ;
		var lnTotalCount = 0 ;
		
		var func_name = "f" + d.pid + "_" + d.time_int ;
		console.log(func_name) ;
		proc_data = executeFunctionByName(func_name, window, "");
		
					

			///Files Created
			var files_created = proc_data.files_created;
			var fileop_count = 0 ;
			lncount = 0 ;
			lnTotalCount = 0 ;

			filecreate_data.length = 0;
			filecreate_datatable.clear().draw() ;
			var file_bytes = 0 ;
			for(var i in files_created)
			{
				++lnTotalCount ;
				if (summary_mode && files_created[i].summary == 0)
					continue ;
				
				file_bytes = (files_created[i].bytes == -1) ? null : files_created[i].bytes ;
				filecreate_data.push([files_created[i].file_name, files_created[i].file_path, file_bytes, files_created[i].time_int]);
				++lncount ;
			}			
			filecreateLbl.html(create_radio_prefix + "(" + (lncount) + "\\" + lnTotalCount +")") ;
			fileop_count = fileop_count + lncount ;
			filecreate_datatable.rows.add(filecreate_data).draw() ;

			///Files Renamed
			var files_renamed = proc_data.files_renamed;

			lncount = 0 ;
			lnTotalCount = 0 ;
			filerename_data.length = 0;
			filerename_datatable.clear().draw() ;
			for(var i in files_renamed)
			{
				++lnTotalCount ;
				if (summary_mode && files_renamed[i].summary == 0)
					continue ;

				file_bytes = (files_renamed[i].bytes == -1) ? null : files_renamed[i].bytes ;
				filerename_data.push([files_renamed[i].file_name, files_renamed[i].file_path, files_renamed[i].source_name, files_renamed[i].source_path, file_bytes, files_renamed[i].time_int]) ;
				++lncount ;
			}	
			filerenameLbl.html(rename_radio_prefix + "(" + (lncount) + "\\" + lnTotalCount +")") ;
			filerename_datatable.rows.add(filerename_data).draw() ;
			fileop_count = fileop_count + lncount ;
			
			///Files Deleted
			var files_deleted = proc_data.files_deleted;

			lncount = 0 ;
			lnTotalCount = 0 ;
			filedelete_data.length = 0;
			filedelete_datatable.clear().draw() ;
			for(var i in files_deleted)
			{
				++lnTotalCount ;
				if (summary_mode && files_deleted[i].summary == 0)
					continue ;

				file_bytes = (files_deleted[i].bytes == -1) ? null : files_deleted[i].bytes ;
				filedelete_data.push([files_deleted[i].file_name, files_deleted[i].file_path, file_bytes, files_deleted[i].time_int]);
				++lncount ;
			}
			filedeleteLbl.html(delete_radio_prefix + "(" + (lncount) + "\\" + lnTotalCount +")") ;
			filedelete_datatable.rows.add(filedelete_data).draw() ;
			fileop_count = fileop_count + lncount ;

			///Files Written
			var files_written = proc_data.files_written;

			lncount = 0 ;
			lnTotalCount = 0 ;
			filewrite_data.length = 0;
			filewrite_datatable.clear().draw() ;
			for(var i in files_written)
			{
				++lnTotalCount ;
				if (summary_mode && files_written[i].summary == 0)
					continue ;

				file_bytes = (files_written[i].bytes == -1) ? null : files_written[i].bytes ;
				filewrite_data.push([files_written[i].file_name, files_written[i].file_path, file_bytes, files_written[i].time_int]);						
				++lncount ;
			}	
			filewriteLbl.html(write_radio_prefix + "(" + (lncount) + "\\" + lnTotalCount +")") ;
			filewrite_datatable.rows.add(filewrite_data).draw() ;
			fileop_count = fileop_count + lncount ;

			///Files opened
			var files_read = proc_data.files_read;

			lncount = 0 ;
			lnTotalCount = 0 ;
			fileread_data.length = 0;
			fileread_datatable.clear().draw() ;
			for(var i in files_read)
			{
				++lnTotalCount ;
				if (summary_mode && files_read[i].summary == 0)
					continue ;

				file_bytes = (files_read[i].bytes == -1) ? null : files_read[i].bytes ;
				fileread_data.push([files_read[i].file_name, files_read[i].file_path, file_bytes, files_read[i].time_int]);
				++lncount ;
			}
			filereadLbl.html(read_radio_prefix + "(" + (lncount) + "\\" + lnTotalCount +")") ;
			fileread_datatable.rows.add(fileread_data).draw() ;
			fileop_count = fileop_count + lncount ;

			$(fileop_tab).text(fileop_tab_prefix + "(" + fileop_count +")") ;

			var netop_count = 0 ;
			///URLs Accessed
			var urls_accessed = proc_data.http_accessed;

			lncount = 0 ;
			lnTotalCount = 0 ;
			url_data.length = 0 ;
			url_datatable.clear().draw() ;
			for(var i in urls_accessed)
			{
				++lnTotalCount ;
				if (summary_mode && urls_accessed[i].summary == 0)
					continue ;
			
				url_data.push([urls_accessed[i].protocol, urls_accessed[i].url, urls_accessed[i].action, urls_accessed[i].rev_dns, urls_accessed[i].dst_ip, urls_accessed[i].dst_port, urls_accessed[i].src_ip, urls_accessed[i].src_port, urls_accessed[i].time_int]);
				++lncount ;
			}
			nethttpLbl.html("HTTP/HTTPS: " + "(" + (lncount) + "\\" + lnTotalCount +")") ;				
			url_datatable.rows.add(url_data).draw() ;
			netop_count = netop_count + lncount ;

			
			///IANA Accessed
			var iana_accessed = proc_data.iana_accessed;

			lncount = 0 ;
			lnTotalCount = 0 ;
			iana_data.length = 0;
			iana_datatable.clear().draw() ;
			for(var i in iana_accessed)
			{
				++lnTotalCount ;
				if (summary_mode &&iana_accessed[i].summary == 0)
					continue ;
				iana_data.push([iana_accessed[i].protocol, iana_accessed[i].app_protocol, iana_accessed[i].rev_dns, iana_accessed[i].dst_ip, iana_accessed[i].dst_port, iana_accessed[i].src_ip, iana_accessed[i].src_port, iana_accessed[i].time_int]);
				++lncount ;
			}
			netianaLbl.html("Other: " + "(" + (lncount) + "\\" + lnTotalCount +")") ;
			iana_datatable.rows.add(iana_data).draw();
			netop_count = netop_count + lncount ;				

			$(netop_tab).text(netop_tab_prefix + "(" + netop_count +")") ;

			///Reg Read Ops
			var reg_read_ops = proc_data.reg_read;
			var regop_count = 0 ;
			
			lncount = 0 ;
			lnTotalCount = 0 ;
			regread_data.length = 0;
			regread_datatable.clear().draw() ;
			for(var i in reg_read_ops)
			{
				++lnTotalCount ;
				if (summary_mode && reg_read_ops[i].summary == 0)
					continue ;
				if (reg_read_ops[i].reg_data_new == null)
					regread_data.push([reg_read_ops[i].reg_key, reg_read_ops[i].reg_action, reg_read_ops[i].reg_value, "", reg_read_ops[i].time_int]) ;
				else
					regread_data.push([reg_read_ops[i].reg_key, reg_read_ops[i].reg_action, reg_read_ops[i].reg_value, reg_read_ops[i].reg_data_new, reg_read_ops[i].time_int]) ;
				++lncount ;
			}	
			regreadLbl.html(read_radio_prefix + "(" + (lncount) + "\\" + lnTotalCount +")") ;
			regread_datatable.rows.add(regread_data).draw();				
			regop_count = regop_count + lncount ;
			
			///Reg Write Ops
			var reg_write_ops = proc_data.reg_modified;

			lncount = 0 ;
			lnTotalCount = 0 ;
			regwrite_data.length = 0;
			regwrite_datatable.clear().draw() ;
			for(var i in reg_write_ops)
			{
				++lnTotalCount ;
				if (summary_mode && reg_write_ops[i].summary == 0)
					continue ;

				if (reg_write_ops[i].reg_data_new == null &&  reg_write_ops[i].reg_data_old == null)
					regwrite_data.push([reg_write_ops[i].reg_key, reg_write_ops[i].reg_action, reg_write_ops[i].reg_value, "", "", reg_write_ops[i].time_int]);
				else
					regwrite_data.push([reg_write_ops[i].reg_key, reg_write_ops[i].reg_action, reg_write_ops[i].reg_value, reg_write_ops[i].reg_data_old, reg_write_ops[i].reg_data_new, reg_write_ops[i].time_int]) ;
				
				++lncount ;
			}	
			regwriteLbl.html(write_radio_prefix + "(" + (lncount) + "\\" + lnTotalCount +")") ;
			regwrite_datatable.rows.add(regwrite_data).draw();
			regop_count = regop_count + lncount ;
		
			$(regop_tab).text(regop_tab_prefix + "(" + regop_count +")") ;
			
			///Injection Ops
			var injection_ops = proc_data.injected;

			lncount = 0 ;
			lnTotalCount = 0 ;
			injecthook_data.length = 0;
			injecthook_datatable.clear().draw() ;
			for(var i in injection_ops)
			{
				++lnTotalCount ;
				if (summary_mode && injection_ops[i].summary == 0)
					continue ;
				
				if (injection_ops[i].result == null)
					injecthook_data.push([injection_ops[i].target_path, injection_ops[i].technique, injection_ops[i].description, "", injection_ops[i].time_int]) ;
				else
					injecthook_data.push([injection_ops[i].target_path, injection_ops[i].technique, injection_ops[i].description, injection_ops[i].result, injection_ops[i].time_int]) ;
				++lncount ;
			}	
			injecthook_datatable.rows.add(injecthook_data).draw();
			
			$(inj_tab).text(inj_tab_prefix + "(" + (lncount) +")") ;				

			///Special Events
			var special_events = proc_data.special_events;
			se_handle(special_events, se_tab_prefix) ;		
			
			///Damage Events
			var damage_events = proc_data.damage;

			damage_data.length = 0;
			damage_datatable.clear().draw() ;			
			
			lncount = 0 ;
			lnTotalCount = 0 ;
			for(var i in damage_events)
			{
				damage_data.push([damage_events[i].dmg_resource, damage_events[i].dmg_impact, damage_events[i].time_int]);			
				++lncount ;
			}

			damage_datatable.rows.add(damage_data).draw() ;		
			$(dmg_tab).text(dmg_tab_prefix + "(" + (lncount) +")") ;
			
			if (draw_creation_links)
				drawCreationLines(d, proc_data) ;
		
	}

	function nodeLabel(d, treetimeline) {
		if (d.type == 'proc')
			return d.name + " " + d.pid + "\n" + d.tooltip ;
		else if (d.type == 'boot' && d.parent.type != 'boot')
		{
			if (treetimeline)
				return " " + "\n" + d.tooltip ;
			else
				return getDateTimeString(d.time_int) + "\n" + d.tooltip ;
		}
		else if (d.type == 'file')
			return d.name + "\n" + d.tooltip ;
		else
			return "" ;
	}

	function drawSubtreeLinks(link)
	{
		var loSourceNode = findProcNode(link.source_pid, link.source_time) ;
		if (loSourceNode == null)
		{
			alert ("source node not found") ;
			return ;
		}
		
		var loTargetNode = findProcNode(link.target_pid, link.target_time) ;
		if (loTargetNode == null)
		{
			alert ("target node not found") ;
			return ;
		}
		
		svgGroup.append("path", "g")
			.attr("class", "subtreeLinks")
			.style("stroke-dasharray", ("2, 8"))
			.attr("d", function() {
				var sTarget = {
					x: loTargetNode.x0 ,
					y: loTargetNode.y0 + radius_exclude_transparent
				};
				var sSource = {
					x: loSourceNode.x0 + radius_exclude_transparent,
					y: loSourceNode.y0
				};

				return ("M" + sSource.y + "," + sSource.x + "L" + sTarget.y + "," + sTarget.x) ;
			});
	}
	
	function drawInjectionLinks(link)
	{
		var loSourceNode = findProcNode(link.source_pid, link.source_time) ;
		if (loSourceNode == null)
		{
			console.log("source node not found") ;
			return ;
		}
		
		var loTargetNode = findProcNode(link.target_pid, link.target_time) ;
		if (loTargetNode == null)
		{
			console.log("target node not found") ;
			return ;
		}
		
		svgGroup.append("path", "g")
				.attr("class", "injectionLinks")
				.attr("d", function() {
					var sTarget = {
						//x: loTargetNode.x0 + radius_exclude_transparent,
						//y: loTargetNode.y0
						x: loTargetNode.x0 + radius_exclude_transparent * Math.cos(80),
						y: loTargetNode.y0 + radius_exclude_transparent * Math.sin(80)						
					};
					var sSource = {
						//x: loSourceNode.x0 + radius_exclude_transparent,
						//y: loSourceNode.y0
						x: loSourceNode.x0 + radius_exclude_transparent * Math.cos(-80),
						y: loSourceNode.y0 + radius_exclude_transparent * Math.sin(-80)						
					};
					
					return "M " + sSource.y + " " + sSource.x + " L " + sTarget.y + " " + sTarget.x ;  
					//return draw_curve(sSource.y, sSource.x, sTarget.y, sTarget.x, 35) ;
				});
	}
		
	function showSelector(Ax, Ay)
	{
		var allSelectors = document.getElementsByClassName("nodeOpSelector");
		$(allSelectors).remove();
		var opSelector = 	svgGroup.append("path", "g")
				.attr("class", "nodeOpSelector")
				.attr("stroke", "#455A7A")
				.attr("fill", "none")
				.attr("stroke-width", 2)
				.attr("d", draw_Selector(Ax, Ay));			
	}
	
	function draw_Selector(Ax, Ay)
	{
		var start_draw_x = Ay - radius_default ;
		var start_draw_y = Ax + radius_default + selector_below ;
		var end_draw_x = Ay + radius_default ;
		var end_draw_y = start_draw_y ;
		return "M " + start_draw_x + " " + start_draw_y + " L " + end_draw_x + " " + end_draw_y ;  
	}

	function showElevatedProcs()
	{	
		var allOpIndicators = document.getElementsByClassName("nodeElevated");
		$(allOpIndicators).remove();
		
		all_nodes.forEach(function(loNode) {
				if (loNode.type != "proc")
					return ;
				
				if (loNode.sid != start_sid)
					createElevated(loNode.y0, loNode.x0) ;
				return ;
		});
	}
	
	function createElevated(Ax, Ay)
	{
		var opSelector = 	svgGroup.append("path", "g")
				.attr("class", "nodeElevated")
				.attr("stroke", "#E06767")
				.attr("fill", "none")
				.attr("stroke-width", 2)
				.attr("d", draw_Arrow(Ax, Ay));			
	}		
	
	function draw_Arrow(Ax, Ay)
	{
		var point_head_x = Ax + radius_default + 6 ;
		var point_head_y = Ay - radius_default  ;
		var start_draw_x = point_head_x - 4 ;
		var start_draw_y = point_head_y + 4 ;
		var end_draw_x = point_head_x + 4 ;
		var end_draw_y = point_head_y + 4 ;
		var arrow_floor_x = point_head_x ;		
		var arrow_floor_y = Ay - 10 ;
		return "M " + start_draw_x + " " + start_draw_y + " L " + point_head_x + " " + point_head_y + " L " + end_draw_x + " " + end_draw_y  + " M " + point_head_x + " " + point_head_y + " L " + arrow_floor_x + " " + arrow_floor_y;  
	}		
		
	function findProcNode(tnPID, tnCreationTime)
	{		
		for (i = 0; i < all_nodes.length; ++i)
		{
			if (all_nodes[i].type != 'proc')
				continue ;
				
			if (all_nodes[i].pid == tnPID && all_nodes[i].time_int == tnCreationTime)
			{
				return all_nodes[i] ;
			}
		}			
		
		return null ;
	}	
	
	function drawCreationLines(d, proc_data)
	{
		var parentCreationEdges = document.getElementsByClassName("additionalParentLink");
		$(parentCreationEdges).remove();
		var childCreationEdges = document.getElementsByClassName("additionalChildLink");
		$(childCreationEdges).remove();
		
		all_nodes.forEach(function(loNode) {
				if (d.created != "" && loNode.path == d.created && loNode.pid == d.created_pid)
				{
					svgGroup.append("path", "g")
							.attr("class", "additionalParentLink")
							.style("stroke-dasharray", ("3, 6"))
							.attr("d", function() {
								var oTarget = {
									x: d.x0,
									y: d.y0 - radius_exclude_transparent
								};
								var oSource = {
									x: loNode.x0,
									y: loNode.y0 + radius_exclude_transparent
								};
								
								return draw_curve(oSource.y, oSource.x, oTarget.y, oTarget.x, 35) ;		
							});
				}
				
				if (d.files_created_count > 0)
				{
					for (var i in proc_data.files_created)
					{
						if (loNode.path == proc_data.files_created[i].file_path)
						{
							svgGroup.append("path", "g")
							.attr("class", "additionalChildLink")
							.style("stroke-dasharray", ("3, 6"))
							.attr("d", function() {
								var oTarget = {
									x: d.x0,
									y: d.y0 + radius_exclude_transparent
								};
								var oSource = {
									x: loNode.x0,
									y: loNode.y0 - radius_exclude_transparent
								};
								return draw_curve(oSource.y, oSource.x, oTarget.y, oTarget.x, 35) ;									
							});								
						}
					}
				}
		});	
	}

	function showOpsIndicator()
	{	
		var allOpIndicators = document.getElementsByClassName("nodeOpIndicator");
		$(allOpIndicators).remove();
		
		all_nodes.forEach(function(loNode) {
				if (loNode.type != "proc")
					return ;
				
				var opCount = 0 ;
				
				switch (mySelectedTab)
				{
					case myTabs.Proc :
					case myTabs.ProcSec :
						return ;
					case myTabs.FileOps:
						if (selected_file_div == div_file_create)
						{
							opCount = (summary_mode ? loNode.files_created_s_count : loNode.files_created_count) ;
							break ;
						}
						else if (selected_file_div == div_file_rename)
						{
							opCount = (summary_mode ? loNode.files_renamed_s_count : loNode.files_renamed_count) ;
							break ;
						}
						else if (selected_file_div == div_file_delete)
						{
							opCount = (summary_mode ? loNode.files_deleted_s_count : loNode.files_deleted_count) ;
							break ;
						}
						else if (selected_file_div == div_file_write)
						{
							opCount = (summary_mode ? loNode.files_written_s_count : loNode.files_written_count) ;
							break ;
						}
						else if (selected_file_div == div_file_read)
						{
							opCount = (summary_mode ? loNode.files_read_s_count : loNode.files_read_count) ;
							break ;
						}
						else
						{
							console.log("File Op Not Found") ;
							break ;
						}
					case myTabs.NetOp :
						if (selected_net_div == div_net_http)
						{
							opCount = (summary_mode ? loNode.urls_accessed_s_count : loNode.urls_accessed_count) ;
							break ;
						}
						else if (selected_net_div == div_net_other)
						{
							opCount = (summary_mode ? loNode.iana_accessed_s_count : loNode.iana_accessed_count) ;
							break ;
						}
						else
						{
							console.log("Network Op Not Found") ;
							break ;
						}
					case myTabs.RegOps :
						if (selected_reg_div == div_reg_write)
						{
							opCount = (summary_mode ? loNode.reg_modified_s_count : loNode.reg_modified_count) ;
							break ;
						}
						else if (selected_reg_div == div_reg_read)
						{
							opCount = (summary_mode ? loNode.reg_read_s_count : loNode.reg_read_count) ;
							break ;
						}
						else
						{
							console.log("Reg Op Not Found") ;
							break ;
						}
					case myTabs.InjectOps :
							opCount = (summary_mode ? loNode.injected_s_count : loNode.injected_count) ;
							break ;					
					case myTabs.SpecialEvents :
							opCount = loNode.special_events_count ;
							break ;
					case myTabs.Damage :
							opCount = loNode.damage_count ;
							break ;
					default:
						return ;
				}
				
				if (opCount == 0)
					return ;
				createOpsIndicator(loNode.x0, loNode.y0) ;
				return ;
		});
	}
	
	function createOpsIndicator(Ax, Ay)
	{
		var opIndicator = 	svgGroup.append("path", "g")
							.attr("class", "nodeOpIndicator")
							.attr("stroke", "#455A7A")
							.attr("fill", "none")
							.attr("stroke-width", 2)
							.attr("d", draw_Tilde(Ax, Ay));	
	}
	
	function draw_Tilde(Ax, Ay)
	{
		var point_head = Ax + radius_default + selector_below + 4 ;
		var start_draw_x = Ay - 5 ;
		var start_draw_y = point_head + 5 ;
		var end_draw_x = Ay + 5 ;
		var end_draw_y = point_head + 5 ;
		return "M " + start_draw_x + " " + start_draw_y + " L " + Ay + " " + point_head + " L " + end_draw_x + " " + end_draw_y ;  
	}
	
	function draw_curve(Ax, Ay, Bx, By, M) {

		// side is either 1 or -1 depending on which side you want the curve to be on.
		// Find midpoint J
		var Jx = Ax + (Bx - Ax) / 2 ;
		var Jy = Ay + (By - Ay) / 2 ;
		

		// We need a and b to find theta, and we need to know the sign of each to make sure that the orientation is correct.
		var a = Bx - Ax ;
		var asign = (a < 0 ? -1 : 1) ;
		var b = By - Ay ;
		var bsign = (b < 0 ? -1 : 1) ;
		var theta = Math.atan(b / a) ;

		// Find the point that's perpendicular to J on side
		var costheta = asign * Math.cos(theta) ;
		var sintheta = asign * Math.sin(theta) ;

		// Find c and d
		var c = M * sintheta ;
		var d = M * costheta ;

		// Use c and d to find Kx and Ky
		var Kx = Jx - c ;
		var Ky = Jy + d ;

		return "M" + Ax + "," + Ay +
			   "Q" + Kx + "," + Ky +
			   " " + Bx + "," + By ;
	}
	
	function setupProcElements(svgGroup_param, all_nodes_param, root_sid_param, tree_regular)
	{
		all_nodes = all_nodes_param ;
		svgGroup = svgGroup_param ;
		start_sid = root_sid_param ;
		if (tree_regular)
			viewerHeight = $('#tree-container').height() ; 
		else
			viewerHeight = $('#container_tree_tl').height() ;

		selected_file_div = $('#div_filecreate')[0] ;
		selected_reg_div = $('#div_regwrite')[0] ;
		selected_net_div = $('#div_p_http')[0] ;
	
		sum_button = $('#summary_button') ;
		comp_button = $('#complete_button') ;
		
		div_file_create = $('#div_filecreate')[0] ;
		div_file_rename = $('#div_filerename')[0] ;
		div_file_delete = $('#div_filedelete')[0] ;
		div_file_write = $('#div_filewrite')[0] ;
		div_file_read = $('#div_fileread')[0] ;
		div_net_http = $('#div_p_http')[0] ;
		div_net_other = $('#div_p_other')[0] ;
		div_reg_write = $('#div_regwrite')[0] ;
		div_reg_read = $('#div_regread')[0] ;
		div_text_width = $('#TestText')[0] ;
	
		$('#entry_point_title_tab').on('click', function (event) {
			event.preventDefault() ;		
			window.location.assign("entry_point.html") ;
		});
		
		$('#process_tabs_holder #process_header_tabs a').on('click', function(e)  {
			var currentAttrValue = $(this).attr('href');
 
			// Show/Hide Tabs
			$('#process_tabs_holder ' + currentAttrValue).show().siblings().hide();
 
			// Change/remove current tab to active
			$(this).parent('li').addClass('tab_selected').siblings().removeClass('tab_selected');
			switch (currentAttrValue)
			{
				case "#tab1" :
					mySelectedTab = myTabs.Proc ;
					break ;
				case "#tab2" :
					mySelectedTab = myTabs.ProcSec ;
					break ;
				case "#tab3" :
					mySelectedTab = myTabs.FileOps ;
					break ;
				case "#tab8" :
					mySelectedTab = myTabs.NetOp ;
					break ;
				case "#tab9" :
					mySelectedTab = myTabs.RegOps ;
					break ;
				case "#tab11" :
					mySelectedTab = myTabs.SpecialEvents ;
					break ;
				case "#tab12" :
					mySelectedTab = myTabs.Damage ;
					break ;
				case "#tab13" :
					mySelectedTab = myTabs.Reputation ;
					break ;
				case "#tab14" :
					mySelectedTab = myTabs.InjectOps ;
					break ;			
				default:
					return ;					
			}
			
			showOpsIndicator() ;	
			e.preventDefault();
		});
		
		$("input[name='fileops']").change(function () {
			var file_radio_selected = $('input:radio[name=fileops]:checked') ;
			var checked = file_radio_selected.val();
			selected_file_div.style.display = 'none';
			
			switch (checked)
			{
				case 'f_created':
					selected_file_div = $('#div_filecreate')[0] ;
					break ;
				case 'f_renamed':	
					selected_file_div = $('#div_filerename')[0] ;
					break ;
				case 'f_deleted':	
					selected_file_div = $('#div_filedelete')[0] ;
					break ;
				case 'f_wrote':	
					selected_file_div = $('#div_filewrite')[0] ;
					break ;
				case 'f_read':	
					selected_file_div = $('#div_fileread')[0] ;
					break ;
				default:
					alert('error');				
			}
			selected_file_div.style.display = 'block' ;
			showOpsIndicator() ;
		});		
		
		$("input[name='netops']").change(function () {
			var radio_selected = $('input:radio[name=netops]:checked') ;
			var checked = radio_selected.val();
			selected_net_div.style.display = 'none';
			
			switch (checked)
			{
				case 'n_http':	
					selected_net_div = $('#div_p_http')[0] ;
					break ;
				case 'n_other':	
					selected_net_div = $('#div_p_other')[0] ;
					break ;
				default:
					alert('error');				
			}
			selected_net_div.style.display = 'block' ;
			showOpsIndicator() ;	
		});

		$("input[name='regops']").change(function () {
			var radio_selected = $('input:radio[name=regops]:checked') ;
			var checked = radio_selected.val();
			selected_reg_div.style.display = 'none';
			
			switch (checked)
			{
				case 'r_wrote':	
					selected_reg_div = $('#div_regwrite')[0] ;
					break ;
				case 'r_read':	
					selected_reg_div = $('#div_regread')[0] ;
					break ;
				default:
					alert('error');				
			}
			selected_reg_div.style.display = 'block' ;
			showOpsIndicator() ;	
		});			
		//var allProcNodes = svgGroup.selectAll("g.node") ;
		
		
		$('.circular').on('click',  function(event){
			event.preventDefault();
			legendOverlay() ;
		});
		
		$('#legendImg').on('click',  function(event){
			event.preventDefault();
			legendOverlay() ;
		});	
		
		$('#proc_panel_collapser').on('click',  function(event){
			event.preventDefault();
			if( procPanelState == ProcPanelEnum.MAXIMIZED)
				procInfoMaxRestore();
			else
				procInfoMinRestore();
		});	
		
		$('#proc_panel_expander').on('click',  function(event){
			event.preventDefault();
			if( procPanelState == ProcPanelEnum.MINIMIZED)
			{
				if (selectedNodeData == null)
				{
					alert('Please first select a process to view.') ;
					return ;
				}
				procInfoMinRestore();	
			}
			else
				procInfoMaxRestore();
		});

		$('#summary_button').on('click', function(event) {
			event.preventDefault();
			if (sum_button.hasClass("switch-selected"))
				return ;
			
			comp_button.removeClass('switch-selected') ;
			sum_button.addClass('switch-selected') ;
			
			summary_mode = true ;
			if (selectedNodeData == null)
				return ;
			showNodeText(selectedNodeData, false) ;
			showOpsIndicator() ;		 
		});

		$('#complete_button').on('click', function(event) {
			event.preventDefault();
			if (comp_button.hasClass("switch-selected"))
				return ;
			
			sum_button.removeClass('switch-selected') ;
			comp_button.addClass('switch-selected') ;

			summary_mode = false ;
			if (selectedNodeData == null)
				return ;

			showNodeText(selectedNodeData, false) ;
			showOpsIndicator(mySelectedTab) ;		
		});		
	}
	
	function createSubtreeLinks()
	{
		var stJSON = getSubtreeJSON() ;

		var dataLength = stJSON.length ;
		for (var i =0; i < dataLength; ++i)
		{
			drawSubtreeLinks(stJSON[i]) ;
		}
	}
	
	function createInjectionLinks()
	{
		var injJSON = getInjectionJSON() ;

		var dataLength = injJSON.length ;
		for (var i =0; i < dataLength; ++i)
		{
			drawInjectionLinks(injJSON[i]) ;
		}
	}

	function drawManualElements()
	{
		showElevatedProcs() ;
		createSubtreeLinks() ;
		createInjectionLinks() ;		
	}	
	
	function procInfoMinRestore() {
		if ($('.proc-tab-content').css("display") == "none")
		{
			$('.process').height(regularProcPanelHeight) ;
			$('#proc_panel_collapser').show() ;
			$('.proc-tab-content').show() ;
			procPanelState = ProcPanelEnum.REGULAR ;
		}
		else 
		{
			$('.process').height(minimizedProcPanelHeight) ;
			$('#proc_panel_collapser').hide() ;
			$('.proc-tab-content').hide() ;
			procPanelState = ProcPanelEnum.MINIMIZED ;
		}	
	}

	function procInfoMaxRestore() {
		if (procPanelState == ProcPanelEnum.MAXIMIZED)
		{
			$('.process').height(regularProcPanelHeight) ;
			$('#proc_panel_expander').show() ;
			$('.proc-tab-content').show() ;
			procPanelState = ProcPanelEnum.REGULAR ;
		}
		else 
		{
			$('.process').height(viewerHeight - 20) ;
			$('#proc_panel_expander').hide() ;
			$('.proc-tab-content').show() ;
			procPanelState = ProcPanelEnum.MAXIMIZED ;
		}	
	}
	
	function executeFunctionByName(functionName, context /*, args */) {
	  var args = [].slice.call(arguments).splice(2);
	  var namespaces = functionName.split(".");
	  var func = namespaces.pop();
	  for(var i = 0; i < namespaces.length; i++) {
	    context = context[namespaces[i]];
	  }
	
	  return context[func].apply(this, args);
	}
	
	function loadProcScript(file, d, draw_creation_links) {
	    // DOM: Create the script element
	    var jsElm = document.createElement("script");
	    // set the type attribute
	    jsElm.type = "application/javascript";
	    // set the id for later removal
	    jsElm.id = "procscript" ;
	    // make the script element load file
	    jsElm.src = file;
	    // finally insert the element to the body element in order to load the script
	    jsElm.addEventListener("load", function() { showNodeText(d, draw_creation_links)});
	    document.body.appendChild(jsElm);
	}
	
	function removeProcScript() {
	    // DOM: Create the script element
	    $('#procscript').remove() ;
	}	