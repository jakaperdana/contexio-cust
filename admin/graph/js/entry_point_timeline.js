function startEntryPoint() 
{
	var testData = getEntryPointJSON() ;
	var mainsection = document.getElementById('cd-timeline') ;
	var dataLength = testData.length ;
	console.log("DataLength: " + dataLength) ;
	for (var i =0; i < dataLength ; ++i)
	{
		var iDiv = document.createElement('div');
		if (i == dataLength - 1)
			iDiv.id = 'endblock' ;
		else
			iDiv.id = 'block' + i;
		iDiv.className = 'cd-timeline-block';

		var imageDiv = document.createElement('div');
		imageDiv.className = 'cd-timeline-img cd-picture' ;
		iDiv.appendChild(imageDiv);

		var myImage = document.createElement('img');
		myImage.src = 'images/' + testData[i].image;
		myImage.alt = testData[i].image;
		myImage.width = '32px';
		myImage.height = '32px';
		
		imageDiv.appendChild(myImage) ;
		
		var contentDiv = document.createElement('div');
		contentDiv.id = 'content' + i ;
		contentDiv.className = 'cd-timeline-content';
		iDiv.appendChild(contentDiv);

		var contentHeader = document.createElement('h2');
		contentHeader.innerHTML = testData[i].proc + ' (PID: ' + testData[i].pid + ') ' + testData[i].action ;
		contentDiv.appendChild(contentHeader) ;
		
		var contentText = document.createElement('p');
		if (testData[i].action == 'renames file')
		{
			contentText.innerHTML = testData[i].source + "<br/>to<br/>" + testData[i].resource ;
		}
		else
		{
			if (testData[i].rdns == null || testData[i].rdns == "")
				contentText.innerHTML = testData[i].resource  ;
			else
				contentText.innerHTML = testData[i].resource + "<br/>(Reverse DNS: " + testData[i].rdns + ")";
		}
		contentDiv.appendChild(contentText) ;
		
		//var buttonLink = document.createElement('a') ;
		//buttonLink.href = "#0" ;
		//buttonLink.class = "cd-read-more" ;
		//buttonLink.innerHTML = "Read More" ;
		//contentDiv.appendChild(buttonLink) ;
		
		var contentDate = document.createElement('span');
		contentDate.className = 'cd-date' ;
		contentDate.innerHTML = getDateTimeString(testData[i].start_int) ;
		contentDiv.appendChild(contentDate) ;

		mainsection.appendChild(iDiv);
	}
	
	if (dataLength == 0)
	{
		var noep = document.createElement('h2') ;
		noep.innerHTML = "No Entry Point Data Available" ;
		var noepDiv = document.createElement('div');
		noepDiv.style.textAlign = "center";
		noepDiv.appendChild(noep) ;
		mainsection.appendChild(noepDiv) ;
	}
		
	var selection_list = document.getElementById('ViewSelector') ;		
		
	$('#tree_regular_title_tab').on('click', function (event) {
		event.preventDefault() ;
		window.location.assign("index.html") ;
	});		

	$('#tree_timeline_title_tab').on('click', function (event) {
		event.preventDefault() ;		
		window.location.assign("index_tl.html") ;
	});
}

//Ensure everything is loaded up before running.
$( window ).load( function() {
	startEntryPoint() ; 
});