﻿function showTreeTimeline() 
{
	var treeData = getIncidentJSON() ;
	var radius_default = 24 ;
	
	var text_left_of_node_x = -30 ;
	var text_right_of_node_x = 30 ;
	var text_below_node_y = 12 ;	
	var text_padding = 25 ;
	
	// Influences Height between children nodes 
	var PIXELS_PER_ROW = 80 ;
	// Min width between a parent and child node
	var MIN_PATH_LENGTH = 140 ;
	// Used to store the width of the path for each node.
	var lengthPerNode = [] ;
	
	// Misc. variables
	var i = 0;
	var duration = 750;
	
	var all_nodes = null;
	var fake_root;
	var real_root = null  ;
	var real_root_node = null ;
	
	var selectedNodeData = null ;
	
	var axisHeight = $('#container_axis').height() ;
	var axisNodeY = 20 ;
	
	var newTreeHeight = 0 ; //calculated later.
	// size of the diagram
	var viewerWidth = $('#container_tree_tl').width();
	var viewerHeight = $('#container_tree_tl').height() ; 

	var regularProcPanelHeight = 300 ; //If css changes, this needs to change.
	var effectiveHeight = viewerHeight - regularProcPanelHeight ;
	console.log("Viewer height: " + viewerHeight) ;
	console.log("Effective height: " + effectiveHeight) ;
	console.log("Document height: " + $(document).height()) ;
	
	// size of the diagram
	var axisWidth = $(document).width();
	var axisHeight = axisHeight;
		
	var tree = d3.layout.tree()
		.size([viewerHeight, viewerWidth]);
	
	var timelineTree = d3.layout.tree()
		.size([axisHeight, axisWidth]);	
		
	// define a d3 diagonal projection for use by the node paths later on.
	var diagonal = d3.svg.diagonal()
		.projection(function(d) {
			return [d.y, d.x];
		});

	real_root = visit(treeData, function(d) {
		if (d.type == 'boot')
		{
			delete d.children ;
		}
	}, function(d) {
		if (d.type == 'boot')
		{
			return null;
		}
		return d.children && d.children.length > 0 ? d.children : null;
	});

	// Define the zoom function for the zoomable tree
	function zoom() {
		svgGroup.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
		svgGroupTL.attr("transform", "translate(" + d3.event.translate[0] + ")scale(" + d3.event.scale + ")");
	}

	// define the zoomListener which calls the zoom function on the "zoom" event constrained within the scaleExtents
	var zoomListener = d3.behavior.zoom().scaleExtent([0.2 	, 1.1]).on("zoom", zoom);
	// define the baseSvg, attaching a class for styling and the zoomListener
	var baseSvg = d3.select("#container_tree_tl").append("svg")
		.attr("width", viewerWidth)
		.attr("height", viewerHeight)
		.attr("class", "overlay")
		.style("background-color", "transparent")
		.call(zoomListener);

	var baseSvgTimeline = d3.select("#container_axis").append("svg")
				.attr("width", axisWidth)
				.attr("height", axisHeight)
				.attr("class", "overlay")
				.style("background-color", "transparent");
		
	function drawAxisPath()
	{
		var lnNodeCount = all_nodes.length ;
		if (lnNodeCount <= 1)
			return ;
				
		for (var i = 1 ; i < lnNodeCount -1; ++i)
		{
			svgGroupTL.append("path", "g")
				.attr("class", "link")
				.style("stroke-width", "0.75px") 			
				.attr("d", function() {
					var oSource = {
						x: axisNodeY,
						y: all_nodes[i].y0 + radius_default / 2
					};
					var oTarget = {
						x: axisNodeY,
						y: all_nodes[i+1].y0 - radius_default / 2
					};

					return diagonal({
						source: oSource,
						target: oTarget
					});
				});
		}
	}

	function initialNodePosition(source) {
		scale = zoomListener.scale();
		var graphWidth = 0 ;
		for (j= 0; j < all_nodes.length ; ++j)
		{
			graphWidth = graphWidth + lengthPerNode[j] ;
		}
		
		var x, y ;
		if (graphWidth >= viewerWidth)
		{
			x = radius_default * -2 ;
		}
		else
		{
			x = (viewerWidth / 2) - (graphWidth / 2) ;
		}	

		y = 0 ;

		svgGroup.transition()
			.duration(0)
			.attr("transform", "translate(" + x + "," + y + ")scale(" + scale + ")");
		
		svgGroupTL.transition()
			.duration(0)
			.attr("transform", "translate(" + x + "," + 0 + ")scale(" + scale + ")");
			
		zoomListener.scale(scale);
		zoomListener.translate([x, y]);
	}
	
	function determineDepthPathLengths()
	{
		lengthPerNode.length = all_nodes.length;		

		var nodeLength = 0;
		var realToolTipSize = 0 ;
		var lnToolTipLine1 = 0 ;
		var lsProcInfo, lsTooltipLine1, lsTooltipLine2 ;
		var lsLargerTip, lsLargerText ;
		var computedTextWidth ;

		//determining length of path for each depth of children.
		for (var i = 0; i < all_nodes.length; ++i)
		{
			if (i == 0)
			{
				lengthPerNode[i] = 0 ;
				continue ;
			}
			
			if (all_nodes[i].type == 'boot')
			{
				lengthPerNode[i] = MIN_PATH_LENGTH ;
				continue ;
			}
			lsProcInfo = all_nodes[i].name + " " + all_nodes[i].pid ;
			
			//Now we check to see if tooltip is broken into two lines or 1. If two lines we look at the larger of the two lines length.
			lnToolTipLine1 = all_nodes[i].tooltip.indexOf("\n") ;
			
			if (lnToolTipLine1 == -1)
			{
				realToolTipSize = all_nodes[i].tooltip.length ;
				lsLargerTip = all_nodes[i].tooltip ;
			}
			else
			{
				lsTooltipLine1 = all_nodes[i].tooltip.substr(0, lnToolTipLine1) ;
				lsTooltipLine2 = all_nodes[i].tooltip.substr(lnToolTipLine1 + 1) ;
				var lnSize1 = lsTooltipLine1.length ;
				var lnSize2 = lsTooltipLine2.length ;
				realToolTipSize = Math.max(lnSize1, lnSize2) ;
				lsLargerTip = (lnSize1 > lnSize2) ? lsTooltipLine1 : lsTooltipLine2 ; 
			}
			
			lsLargerText = (lsProcInfo.length > realToolTipSize) ? lsProcInfo : lsLargerTip ;
			computedTextWidth = getTextWidth2(lsLargerText, "12px") ;
			nodeLength = computedTextWidth + radius_default * 2 + text_padding;
			
			// if this is greater than the length of the maximum currently set for this depth, then this becomes the max for this depth
			if (nodeLength > MIN_PATH_LENGTH)
				lengthPerNode[i] = nodeLength ;
			else
				lengthPerNode[i] = MIN_PATH_LENGTH ;
			//console.log("Node: " + lsProcInfo + ", Size: " + lengthPerNode[i]) ;
		}			
	}
	
	function setPathLengths() {
		for (var i = 0; i < all_nodes.length; ++i)
		{
			if (i > 0)
				all_nodes[i].y = all_nodes[i-1].y + lengthPerNode[i] ;
			else
				all_nodes[i].y = lengthPerNode[i] + text_right_of_node_x ; //positive value chosen				
		}
	}
	
	function drawBootLines() {
		
		all_nodes.forEach(function(loNode) {
				if (loNode.type == 'boot')
				{
					svgGroup.append("path", "g")
							.attr("class", "bootLink")						
							.style("stroke-dasharray", ("3, 3"))
							.attr("d", function() {
								var oTarget = {
									x: newTreeHeight-1,
									y: loNode.y0
								};
								var oSource = {
									x: 0 - newTreeHeight,
									y: loNode.y0
								};
								return diagonal({
									source: oSource,
									target: oTarget
								});
							});
				}
		});					
	}
	
	function new_click(d, tonode, initial) {
		if (d.type == 'file')
		{				
			window.open("entry_point.html") ;
			return ;				
		}

		if (d.type != 'proc' )
			return ;
			
		if (d == selectedNodeData)
		{
			if ($('.proc-tab-content').css('display') == "none")
				procInfoMinRestore() ;				
		
			return ;
		}

		dynamicAddProcInfoJS(d)	;
		//showNodeText(d, true) ;
		selectedNodeData = d ;
		showSelector(d.x0, d.y0) ;
		
		if (!initial && $('.proc-tab-content').css('display') == "none")
			procInfoMinRestore() ;
	}

	// Toggle children on double click.	
	function mydblclick(d) {
		//Double click behaviour broken here. Temporarily disabling
		/*
		if (d.type != 'proc')
			return ;
		d = toggleChildren(d);
		update(d);
		centerNode(d);
		*/
	}
	
	var insertLinebreaks = function (d) {
		var el = d3.select(this);
		var words = (nodeLabel(d, true)).split('\n');
		el.text('');

		for (var i = 0; i < words.length; i++) {
			var tspan = el.append('tspan').text(words[i]);
			//tspan.attr('font-size', '12px')
			if (i > 0)
			{
				tspan.attr('x', text_left_of_node_x).attr('dy', text_below_node_y).attr('fill', '#E06767') ;
			}
		}
	};
	
	function elbow(d, i) {
		return "M" + d.source.y + "," + d.source.x
			+ "V" + d.target.x + "H" + d.target.y;
	}
	
	function imageForNode(d, showBoot) {
		if (d.type == 'proc')
			return "images/" + d.image ;
		else if (d.type == 'boot' && showBoot)
			return "images/boot.svg" ;
		
		return "" ;
	}
	
	function update(source) {
		// Compute the new height, function counts total children of fake_root node and sets tree height accordingly.
		// This prevents the layout looking squashed when new nodes are made visible or looking sparse when nodes are removed
		// This makes the layout more consistent.
		var levelWidth = [1];
		var childCount = function(level, n) {

			if (n.children && n.children.length > 0) {
				if (levelWidth.length <= level + 1) levelWidth.push(0);

				levelWidth[level + 1] += n.children.length;
				n.children.forEach(function(d) {
					childCount(level + 1, d);
				});
			}
		};
		
		childCount(0, fake_root);
		newTreeHeight = d3.max(levelWidth) * PIXELS_PER_ROW;
		tree = tree.size([newTreeHeight, viewerWidth]);
		
		// Compute the new tree layout and resort the tree.
		all_nodes = tree.nodes(fake_root) ;
		sortTree(all_nodes) ;
		links = tree.links(all_nodes) ;
		setPathLengths() ;
		
		// Update the nodes…
		node = svgGroup.selectAll("g.node")
			.data(all_nodes, function(d) {
				return d.id || (d.id = ++i);
			});
		
		// Enter any new nodes at the parent's previous position.
		nodeEnter = node.enter().append("g")
			.attr("class", "node")
			.on('dblclick', mydblclick)
			.on('click', function(d) {
				new_click(d, d3.select(this), false) ;
			});
		
		real_root_node = nodeEnter.filter(function(d, i) { return d.real_root == 1 ; }) ;
		
		nodeEnter.append("image")
			.attr("xlink:href", function(d) { return ( (d.damage_count > 0) ?  "images/damage_fire.svg" : ""); })
			.attr("x", radius_default * -1 - 16)
			.attr("y", radius_default * -1 - 25)
			.attr("width", radius_default * 2 + 30)
			.attr("height", radius_default * 2 + 26);

		nodeEnter.append("image")
			.attr("xlink:href", function(d) { return imageForNode(d, false); })
			.attr("x", radius_default * -1)
			.attr("y", radius_default * -1)
			.attr("width", radius_default * 2)
			.attr("height", radius_default * 2);
		
		nodeEnter.append("text")
			.attr("x", text_left_of_node_x)
			.attr("dy", ".35em")
			.attr('class', 'nodeText')
			.attr("text-anchor", "end")
			.text("")
			.style("fill-opacity", 0);
			
		// Transition nodes to their new position.
		var nodeUpdate = node.transition()
			.duration(duration)
			.attr("transform", function(d) {
				return "translate(" + d.y + "," + d.x + ")";
			});

		// Fade the text in
		nodeUpdate.select("text")
			.style("fill-opacity", 1);

		// Transition exiting nodes to the parent's new position.
		var nodeExit = node.exit().transition()
			.duration(duration)
			.attr("transform", function(d) {
				return "translate(" + source.y + "," + source.x + ")";
			})
			.remove();

		nodeExit.select("text")
			.style("fill-opacity", 0);

		// Update the links…
		var link = svgGroup.selectAll("path.link")
			.data(links, function(d) {
				return d.target.id;
			});

		// Enter any new links at the parent's previous position.
		link.enter().insert("path", "g")
			.style("fill", "none")
			.style("stroke", function(d) {
				if (d.source.type == 'fake_root')
					return "transparent" ;
				else if (d.source.type == 'boot')
					return "#3474C5" ;
				else
					return "lightgray" ;						
				})				
			.style("stroke-dasharray", function(d) {
				if (d.source.type == 'boot')
					return ("2, 10") ;
				else
					return ("1, 0") ;
				})	
			.style("stroke-width", function(d) {
				if (d.source.type == 'boot')
					return "0.75px" ;
				else
					return "1px" ;
				})	
			.attr("class", "link")
			.attr("d", function(d) {
				var o = {
					x: source.x0,
					y: source.y0
				};
				return diagonal({
					source: o,
					target: o
				});
			});

		// Transition links to their new position.
		link.transition()
			.duration(duration)
			.attr("d", elbow);

		// Transition exiting nodes to the parent's new position.
		link.exit().transition()
			.duration(duration)
			.attr("d", function(d) {
				var o = {
					x: source.x,
					y: source.y
				};
				return diagonal({
					source: o,
					target: o
				});
			})
			.remove();

		// Stash the old positions for transition.
		all_nodes.forEach(function(d) {
			d.x0 = d.x;
			d.y0 = d.y;
		});
	
		node.selectAll('text').each(insertLinebreaks);
		drawBootLines() ;
		
		updateTL(source) ;
	}
	
	function updateTL(source)
	{
		/// Time Line graph
		var timelineNodes = [];
		var iz = 0;
		all_nodes.forEach(function(d){
			timelineNodes[iz] = jQuery.extend({}, d);
			iz++;
			});
		var count1 = 0;
		timelineNodes.forEach(function(d) {
			if(count1 !=  timelineNodes.length - 1)
				timelineNodes[count1].children = [timelineNodes[count1 + 1]];
			count1++;	
		});

		// Update the nodes…
		timelineNode = svgGroupTL.selectAll("g.node")
			.data(timelineNodes, function(d) {
				return d.id || (d.id = ++i);
			});	
		
		// Enter any new nodes at the parent's previous position.
		timelineNodeEnter = timelineNode.enter().append("g")
			.attr("class", "node")
			
			.attr("transform", function(d) {
				return "translate(" + source.y0 + "," + source.x0 + ")";
			})
			.on('click', function(d) {
				var odds = allProcNodes.filter(function(d1, i) { return d.pid == d1.pid && d.time_int == d1.time_int; });
				new_click(d, odds, false) ;
			});
			//.on('mouseover', showProcessInfo);			

		timelineNodeEnter.append("image")
			.attr("xlink:href", function(d) { return imageForNode(d, true); })
			.attr("x", radius_default / 2 * -1)
			.attr("y", radius_default / 2 * -1)
			.attr("width", radius_default )
			.attr("height", radius_default);							
			
		timelineNodeEnter.append("text")
			.attr("x", 0)
			.attr("dy", "2em")
			.attr('class', 'nodeText')
			.attr("text-anchor", "end")
			.text(function(d) {
				if (d.type == 'fake_root')
					return "" ;
				if (d.type == 'proc' || d.type == 'boot')
					return getDateTimeString(d.time_int) ;
				else
					return "" ;
			})
			.style("fill-opacity", 0);	

		// Transition nodes to their new position.
		var timelineNodeUpdate = timelineNode.transition()
			.duration(duration)
			.attr("transform", function(d) {
				return "translate(" + d.y + "," + axisNodeY + ")";
			});

		// Fade the text in
		timelineNodeUpdate.select("text")
			.style("fill-opacity", 1);

		// Transition exiting nodes to the parent's new position.
		var timelineNodeExit = timelineNode.exit().transition()
			.duration(duration)
			.attr("transform", function(d) {
				return "translate(" + source.y + "," + source.x + ")";
			})
			.remove();

		timelineNodeExit.select("text")
			.style("fill-opacity", 0);

		//timelineLinks = tree.links(timelineNodes);

		// Stash the old positions for transition.
		timelineNodes.forEach(function(d) {
			d.x0 = d.x;
			d.y0 = d.y;
		});			
	}

	// Append a group which holds all nodes and which the zoom Listener can act upon.
	var svgGroupTL = baseSvgTimeline.append("g");
	var svgGroup = baseSvg.append("g");

	// Define the fake_root
	fake_root = treeData;
	fake_root.x0 = viewerHeight / 2;
	fake_root.y0 = 0;
	
	all_nodes = tree.nodes(fake_root);
	sortTree(all_nodes) ;
	console.log("Tree Node Count: " + all_nodes.length) ;
	
	createDataTables() ;	
	setupProcElements(svgGroup, all_nodes, fake_root.sid, false) ;		
	determineDepthPathLengths() ;
	
	update(fake_root);
	drawAxisPath() ;

	if (real_root != null)
	{
		new_click(real_root, real_root_node, true);
		initialNodePosition(real_root) ;
	}
	
	drawManualElements() ;
	
	$('#tree_regular_title_tab').on('click', function (event) {
		event.preventDefault() ;
		window.location.assign("index.html") ;
	});
	
	var allProcNodes = svgGroup.selectAll("g.node") ;
}

//Ensure everything is loaded up before running.
$( window ).load( function() { 
	showTreeTimeline() ;
});