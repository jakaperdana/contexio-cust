var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
var days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
function getDateTimeString(data_date)
{
	if (data_date == 0)
		return null ;
	//console.log(data_date) ;
	var alpha_date = new Date(data_date) ;
	return alpha_date.toLocaleString() ;
	/*
	var year = alpha_date.getFullYear();
	//var day = days[alpha_date.getDay()] ;
	var month = months[alpha_date.getMonth()];
	var date = alpha_date.getDate();
	var hour = alpha_date.getHours();
	var min = alpha_date.getMinutes();
	var sec = alpha_date.getSeconds();
	var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
	return time;
	*/
}

function getDuration(start_date, end_date)
{
	if (start_date >= end_date)
		return "" ;
	
	var lnDurationMS = end_date - start_date ;
	if (lnDurationMS < 1000)
		return lnDurationMS + "ms" ;
	else if (lnDurationMS < 60000)
	{
		var lsSec = Math.floor(lnDurationMS / 1000) + "s " ;
		var lsMs = (lnDurationMS % 1000) + "ms" ;
		return lsSec + lsMs ;
	}
	else if (lnDurationMS < 3600000)
	{
		var lsMin = Math.floor(lnDurationMS / 60000) + "min " ;
		var lsSec = Math.round((lnDurationMS % 60000) / 1000) + "s" ;
		return lsMin + lsSec ;
	}
	else 
	{
		var lsHours = Math.floor(lnDurationMS / 3600000) + "h " ;
		var lsMinutes = Math.round((lnDurationMS % 3600000) / 60000) + "min" ;
		return lsHours + lsMinutes ;
	}
}

function setDetailsSummaryShim()
{	
	details_shim.init=function(){for(var a=document.getElementsByTagName("summary"),b=0;b<a.length;b++) {details_shim(a[b]); }};
	details_shim.init() ;
}