﻿function showTreeRegular() 
{
	var treeData = getIncidentJSON() ;
	var radius_default = 24 ;
	
	var text_left_of_node_x = -30 ;
	var text_right_of_node_x = 30 ;
	var text_below_node_y = 12 ;	
	var text_padding = 25 ;
	
	// Influences Height between children nodes 
	var PIXELS_PER_ROW = 100 ;
	// Min width between a parent and child node
	var MIN_PATH_LENGTH = 140 ;
	// Used to store the width of the path for each depth (parent -> children).
	var maxLengthPerDepth = [] ;
	
	// Misc. variables
	var i = 0;
	var duration = 750;
	
	var all_nodes = null;
	var fake_root;
	var real_root = null  ;
	var real_root_node = null ;
	var pnMaxDepth = 1 ;
	var pnMaxProcDepth = 1 ;
	
	var selectedNodeData = null ;
	
	var newTreeHeight = 0 ; //calculated later.
	// size of the diagram
	var viewerWidth = $('#tree-container').width();
	var viewerHeight = $('#tree-container').height() ; 

	var regularProcPanelHeight = 300 ; //If css changes, this needs to change.
	var minimizedProcPanelHeight = 53 ; //If css changes, this needs to change.
	var startHeight = viewerHeight - minimizedProcPanelHeight ;
	var effectiveHeight = viewerHeight - regularProcPanelHeight ;
	console.log("Viewer height: " + viewerHeight) ;
	console.log("Effective height: " + effectiveHeight) ;
	console.log("Document height: " + $(document).height()) ;
	console.log("Start height: " + startHeight) ;
					
	var tree = d3.layout.tree()
		.size([viewerHeight, viewerWidth]);
		
	// define a d3 diagonal projection for use by the node paths later on.
	var diagonal = d3.svg.diagonal()
		.projection(function(d) {
			return [d.y, d.x];
		});

	real_root = visit(treeData, function(d) {
	}, function(d) {
		return d.children && d.children.length > 0 ? d.children : null;
	});

	// Define the zoom function for the zoomable tree
	function zoom() {
		svgGroup.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
	}

	// define the zoomListener which calls the zoom function on the "zoom" event constrained within the scaleExtents
	var zoomListener = d3.behavior.zoom().scaleExtent([0.1, 3]).on("zoom", zoom);
	
	// define the baseSvg, attaching a class for styling and the zoomListener
	var baseSvg = d3.select("#tree-container").append("svg")
		.attr("width", viewerWidth)
		.attr("height", viewerHeight)
		.attr("class", "overlay")
		.style("background-color", "transparent")
		.call(zoomListener);

	function initialNodePosition(source) {
		scale = zoomListener.scale();
		var graphWidth = 0 ;
		for (j= 0; j < pnMaxDepth ; ++j)
			graphWidth = graphWidth + maxLengthPerDepth[j] ;
		
		var x, y ;
		if (graphWidth >= viewerWidth)
		{
			x = radius_default * -2 ;
		}
		else
		{
			x = (viewerWidth / 2) - (graphWidth / 2) ;
		}	

		if (newTreeHeight > startHeight)
			y = 0 ;
		else
		{
			y = (startHeight / 2) - (newTreeHeight / 2) ;
		}
		
		svgGroup.transition()
			.duration(0)
			.attr("transform", "translate(" + x + "," + y + ")scale(" + scale + ")");
		
		zoomListener.scale(scale);
		zoomListener.translate([x, y]);
	}
	
	function determineDepthPathLengths()
	{
		for (var i = 0; i < all_nodes.length; ++i)
		{
			if (all_nodes[i].type == 'proc' && all_nodes[i].depth > pnMaxProcDepth)
				pnMaxProcDepth = all_nodes[i].depth ;
			
			if (all_nodes[i].depth > pnMaxDepth)
				pnMaxDepth = all_nodes[i].depth ;
		}
		
		//Depths start from 0 so actual depth count = lnMaxDepth + 1
		++pnMaxDepth ;
		++pnMaxProcDepth ;
		
		//Setting array size to max depth
		maxLengthPerDepth.length = pnMaxDepth ;
		//Initializing array with each depth having a minimum of 100 pixels width.
		for (j = 0; j < pnMaxDepth; ++j)
		{
			if (j == 0)
				maxLengthPerDepth[j] = 0;
			else
				maxLengthPerDepth[j] = MIN_PATH_LENGTH ;
		}

		var lnDepth = 0 ;
		var nodeLength = 0;
		var realToolTipSize = 0 ;
		var lnToolTipLine1 = 0 ;
		var lsProcInfo, lsTooltipLine1, lsTooltipLine2 ;
		var lsLargerTip, lsLargerText ;
		var computedTextWidth ;

		//determining length of path for each depth of children.
		for (var i = 0; i < all_nodes.length; ++i)
		{
			lnDepth = all_nodes[i].depth ;
			if (all_nodes[i].type != 'proc')
				continue ;
			
			// If the nodes are at the end of the tree (leafs + max depth) then don't worry about calculating
			//if (all_nodes[i].depth == pnMaxProcDepth -1 )
				//continue ;
			
			lsProcInfo = all_nodes[i].name + " " + all_nodes[i].pid ;
			
			//Now we check to see if tooltip is broken into two lines or 1. If two lines we look at the larger of the two lines length.
			lnToolTipLine1 = all_nodes[i].tooltip.indexOf("\n") ;
			
			if (lnToolTipLine1 == -1)
			{
				realToolTipSize = all_nodes[i].tooltip.length ;
				lsLargerTip = all_nodes[i].tooltip ;
			}
			else
			{
				lsTooltipLine1 = all_nodes[i].tooltip.substr(0, lnToolTipLine1) ;
				lsTooltipLine2 = all_nodes[i].tooltip.substr(lnToolTipLine1 + 1) ;
				var lnSize1 = lsTooltipLine1.length ;
				var lnSize2 = lsTooltipLine2.length ;
				realToolTipSize = Math.max(lnSize1, lnSize2) ;
				lsLargerTip = (lnSize1 > lnSize2) ? lsTooltipLine1 : lsTooltipLine2 ; 
			}
			
			lsLargerText = (lsProcInfo.length > realToolTipSize) ? lsProcInfo : lsLargerTip ;
			computedTextWidth = getTextWidth2(lsLargerText, "12px") ;
			nodeLength = computedTextWidth + radius_default * 2 + text_padding;
			
			// if this is greater than the length of the maximum currently set for this depth, then this becomes the max for this depth
			if (nodeLength > maxLengthPerDepth[lnDepth])
				maxLengthPerDepth[lnDepth] = nodeLength ;
		}			
	}
	
	function setPathLengths() {
		for (var i = 0; i < all_nodes.length; ++i)
		{
			if (all_nodes[i].depth > 0)
				all_nodes[i].y = all_nodes[i].parent.y + maxLengthPerDepth[all_nodes[i].depth] ;
			else
				all_nodes[i].y = maxLengthPerDepth[all_nodes[i].depth] + text_right_of_node_x ; //positive value chosen	
		}
	}
	
	function new_click(d, tonode, initial, treeRegular) {
		if (d.type == 'file')
		{				
			window.open("entry_point.html") ;
			return ;				
		}

		if (d.type != 'proc' )
			return ;
			
		if (d == selectedNodeData)
		{
			if ($('.proc-tab-content').css('display') == "none")
				procInfoMinRestore() ;				
		
			return ;
		}
		
		dynamicAddProcInfoJS(d)	;
		//showNodeText(d, true) ;
		selectedNodeData = d ;
		showSelector(d.x0, d.y0) ;
		
		if (!initial)
		{
			centerNode(d, zoomListener, viewerWidth, effectiveHeight, duration);
			if ($('.proc-tab-content').css('display') == "none")
				procInfoMinRestore() ;
		}
	}

	// Toggle children on double click.	
	function mydblclick(d) {
		//Double click behaviour broken here. Temporarily disabling
		/*
		if (d.type != 'proc')
			return ;
		d = toggleChildren(d);
		update(d);
		centerNode(d);
		*/
	}
	
	var insertLinebreaks = function (d) {
		var el = d3.select(this);
		var words = (nodeLabel(d, false)).split('\n');
		el.text('');

		for (var i = 0; i < words.length; i++) {
			var tspan = el.append('tspan').text(words[i]);
			//tspan.attr('font-size', '12px')
			if (i > 0)
			{
				tspan.attr('x', text_left_of_node_x).attr('dy', text_below_node_y).attr('fill', '#E06767') ;
			}
		}
	};
	
	function imageForNode(d) {
		if (d.type == 'proc')
			return "images/" + d.image ;
		else if (d.type == 'boot' && d.parent.type != 'boot')
			return "images/boot.svg" ;
		
		return "" ;
	}
	
	function update(source) {
		// Compute the new height, function counts total children of fake_root node and sets tree height accordingly.
		// This prevents the layout looking squashed when new nodes are made visible or looking sparse when nodes are removed
		// This makes the layout more consistent.
		var levelWidth = [1];
		var childCount = function(level, n) {

			if (n.children && n.children.length > 0) {
				if (levelWidth.length <= level + 1) levelWidth.push(0);

				levelWidth[level + 1] += n.children.length;
				n.children.forEach(function(d) {
					childCount(level + 1, d);
				});
			}
		};
		
		childCount(0, fake_root);
		newTreeHeight = d3.max(levelWidth) * PIXELS_PER_ROW;
		tree = tree.size([newTreeHeight, viewerWidth])
				   .separation( function(a, b) {
				   return 1; });
		
		// Compute the new tree layout and resort the tree.
		all_nodes = tree.nodes(fake_root) ;
		sortTree(tree) ;
		links = tree.links(all_nodes) ;
		setPathLengths();
		
		// Update the nodes…
		node = svgGroup.selectAll("g.node")
			.data(all_nodes, function(d) {
				return d.id || (d.id = ++i);
			});
		
		// Enter any new nodes at the parent's previous position.
		nodeEnter = node.enter().append("g")
			.attr("class", "node")
			.on('dblclick', mydblclick)
			.on('click', function(d) {
				new_click(d, d3.select(this), false) ;
			});
		
		real_root_node = nodeEnter.filter(function(d, i) { return d.real_root == 1 ; }) ;
		
		nodeEnter.append("image")
			.attr("xlink:href", function(d) { return ( (d.damage_count > 0) ?  "images/damage_fire.svg" : ""); })
			.attr("x", radius_default * -1 - 16)
			.attr("y", radius_default * -1 - 25)
			.attr("width", radius_default * 2 + 30)
			.attr("height", radius_default * 2 + 26);

		nodeEnter.append("image")
			.attr("xlink:href", function(d) { return imageForNode(d); })
			.attr("x", function(d) { return (d.type == 'boot') ? radius_default * -0.5 : radius_default * -1 ;})
			.attr("y", function(d) { return (d.type == 'boot') ? radius_default * -0.5 : radius_default * -1 ;})
			.attr("width", function(d) { return (d.type == 'boot') ? radius_default  : radius_default * 2 ;})
			.attr("height", function(d) { return (d.type == 'boot') ? radius_default  : radius_default * 2 ;});
		
		nodeEnter.append("text")
			.attr("x", text_left_of_node_x)
			.attr("dy", ".35em")
			.attr('class', 'nodeText')
			.attr("text-anchor", "end")
			.text("")
			.style("fill-opacity", 0);
			
		// Transition nodes to their new position.
		var nodeUpdate = node.transition()
			.duration(duration)
			.attr("transform", function(d) {
				return "translate(" + d.y + "," + d.x + ")";
			});

		// Fade the text in
		nodeUpdate.select("text")
			.style("fill-opacity", 1);

		// Transition exiting nodes to the parent's new position.
		var nodeExit = node.exit().transition()
			.duration(duration)
			.attr("transform", function(d) {
				return "translate(" + source.y + "," + source.x + ")";
			})
			.remove();

		nodeExit.select("text")
			.style("fill-opacity", 0);

		// Update the links…
		var link = svgGroup.selectAll("path.link")
			.data(links, function(d) {
				return d.target.id;
			});

		// Enter any new links at the parent's previous position.
		link.enter().insert("path", "g")
			.style("fill", "none")
			.style("stroke", function(d) {
				if (d.source.type == 'fake_root')
					return "transparent" ;
				else if (d.source.type == 'boot')
					return "#3474C5" ;
				else
					return "lightgray" ;						
				})				
			.style("stroke-dasharray", function(d) {
				if (d.source.type == 'boot')
					return ("2, 10") ;
				else
					return ("1, 0") ;
				})	
			.style("stroke-width", function(d) {
				if (d.source.type == 'boot')
					return "0.75px" ;
				else
					return "1px" ;
				})	
			.attr("class", "link")
			.attr("d", function(d) {
				var o = {
					x: source.x0,
					y: source.y0
				};
				return diagonal({
					source: o,
					target: o
				});
			});

		// Transition links to their new position.
		link.transition()
			.duration(duration)
			.attr("d", diagonal);

		// Transition exiting nodes to the parent's new position.
		link.exit().transition()
			.duration(duration)
			.attr("d", function(d) {
				var o = {
					x: source.x,
					y: source.y
				};
				return diagonal({
					source: o,
					target: o
				});
			})
			.remove();

		// Stash the old positions for transition.
		all_nodes.forEach(function(d) {
			d.x0 = d.x;
			d.y0 = d.y;
		});
	
		node.selectAll('text').each(insertLinebreaks);
	}

	// Append a group which holds all nodes and which the zoom Listener can act upon.
	var svgGroup = baseSvg.append("g");

	// Define the fake_root
	fake_root = treeData;
	fake_root.x0 = viewerHeight / 2;
	fake_root.y0 = 0;
	
	all_nodes = tree.nodes(fake_root);
	sortTree(tree) ;
	console.log("Tree Node Count: " + all_nodes.length) ;
	
	createDataTables() ;	
	setupProcElements(svgGroup, all_nodes, fake_root.sid, true) ;		
	determineDepthPathLengths() ;
	
	update(fake_root);
	
	// Layout the tree initially to maximize the number of nodes on screen without scaling.
	if (real_root == null)
	{
		centerNode(fake_root, zoomListener, viewerWidth, effectiveHeight, duration);
	}
	else
	{
		new_click(real_root, real_root_node, true);
		initialNodePosition(real_root) ;
	}
	
	drawManualElements() ;
	
	$('#tree_timeline_title_tab').on('click', function (event) {
		event.preventDefault() ;
		window.location.assign("index_tl.html") ;
	});
}

//Ensure everything is loaded up before running.
$( window ).load( function() { 
	showTreeRegular() ;
});