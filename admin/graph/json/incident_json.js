function getIncidentJSON()
{
var idtJSON =
{
	"type": "fake_root",
	"path": "",
	"real_root": 0,
	"sid": "1000",					//SID - Should be the same as this one (unless we want the arrow up)
	"children": [
				{
					"type": "proc",
					"name": "suspicious",
					"image": "malicious icon.svg",
					"path": "c:\\windows\\system32\\cmd.exe",
					"pid": "n/a",
					"args": "\/c start \"\" \"C:\\Users\\xxxxxx\\Documents\\wnylvtikxvit.exe\"",
					"time_int": 1456906731853,
					"real_root": 1,									// Set to 1 if this is the first
					"md5": "5746bd7e255dd6a8afa06f7c42c1ba41",
					"sig_company": "Microsoft Windows",
					"sid": "1000",
					"execution_calls": "smss.exe (PID: 232  02-Mar-2016 10:16:33) --> smss.exe (PID: 368  02-Mar-2016 10:16:57) --> winlogon.exe (PID: 412  02-Mar-2016 10:16:58) --> userinit.exe (PID: 1252  02-Mar-2016 10:18:20) --> explorer.exe (PID: 2164  02-Mar-2016 10:18:22) --> ",
					"tooltip": "Warning Execution",
					"children": [{
					"type": "proc",
					"name": "suspicious",
					"image": "suspicious.svg",
					"path": "c:\\windows\\system32\\cmd.exe",
					"pid": "n/a",
					"args": "\/c start \"\" \"C:\\Users\\xxxxxx\\Documents\\wnylvtikxvit.exe\"",
					"time_int": 1456906731853,
					"real_root": 1,									// Set to 1 if this is the first
					"md5": "5746bd7e255dd6a8afa06f7c42c1ba41",
					"sig_company": "Microsoft Windows",
					"sid": "1000",
					"execution_calls": "smss.exe (PID: 232  02-Mar-2016 10:16:33) --> smss.exe (PID: 368  02-Mar-2016 10:16:57) --> winlogon.exe (PID: 412  02-Mar-2016 10:16:58) --> userinit.exe (PID: 1252  02-Mar-2016 10:18:20) --> explorer.exe (PID: 2164  02-Mar-2016 10:18:22) --> ",
					"tooltip": "Dangerous Execution",
					"children": [{
					"type": "proc",
					"name": "malicious",
					"image": "malicious.svg",
					"path": "c:\\windows\\system32\\cmd.exe",
					"pid": "n/a",
					"args": "\/c start \"\" \"C:\\Users\\xxxxxx\\Documents\\wnylvtikxvit.exe\"",
					"time_int": 1456906731853,
					"real_root": 1,									// Set to 1 if this is the first
					"md5": "5746bd7e255dd6a8afa06f7c42c1ba41",
					"sig_company": "Microsoft Windows",
					"sid": "1000",
					"execution_calls": "smss.exe (PID: 232  02-Mar-2016 10:16:33) --> smss.exe (PID: 368  02-Mar-2016 10:16:57) --> winlogon.exe (PID: 412  02-Mar-2016 10:16:58) --> userinit.exe (PID: 1252  02-Mar-2016 10:18:20) --> explorer.exe (PID: 2164  02-Mar-2016 10:18:22) --> ",
					"tooltip": "Dangerous Execution",
					"children": [{}]
				}]
				}]
				},
				
				]
};

return idtJSON;
}