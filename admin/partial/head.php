
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Dashboard - Contexio</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
		<link href="css/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<meta name="description" content="">
		<meta name="author" content="">

		<!-- PACE LOAD BAR xPLUGIN - This creates the subtle load bar effect at the top of the page. -->
		<link href="css/plugins/pace/pace.css" rel="stylesheet">
		<script src="js/plugins/pace/pace.js"></script>
		<!-- GLOBAL STYLES - Include these on every page. -->

		<link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">
		<link href="https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i" rel="stylesheet">
		<link href="icons/font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.css" rel="stylesheet">

		<!-- PAGE LEVEL PLUGIN STYLES -->
		<link href="css/plugins/messenger/messenger.css" rel="stylesheet">
		<link href="css/plugins/messenger/messenger-theme-flat.css" rel="stylesheet">
		<link href="css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
		<link href="css/plugins/morris/morris.css" rel="stylesheet">
		<link href="css/plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet">
		<link href="css/plugins/datatables/datatables.css" rel="stylesheet">
		<!-- FOnt ASEM -->
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
		<!-- THEME STYLES - Include these on every page. -->
		<link href="css/style.css" rel="stylesheet">
		<link href="css/plugins.css" rel="stylesheet">
		<link rel="stylesheet" href="css/css/AdminLTE.min.css">
		<!-- AdminLTE Skins. Choose a skin from the css/skins
		folder instead of downloading all of them to reduce the load. -->
		<link rel="stylesheet" href="css/css/skins/_all-skins.min.css">
		<!-- THEME DEMO STYLES - Use these styles for reference if needed. Otherwise they can be deleted. -->
		<link href="css/demo.css" rel="stylesheet">
		<link href="css/test.css" rel="stylesheet">
		<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<script src="js/respond.min.js"></script>
		<![endif]-->
		<style type="text/css">
		.js div#preloader {
		position: fixed;
		left: 0;
		top: 0;
		z-index: 9999;
		width: 100%;
		height: 100%;
		overflow: visible;
		background: #333 url('http://files.mimoymima.com/images/loading.gif') no-repeat center center;
		}
		</style>
		<div class="js"><!--this is supposed to be on the HTML element but codepen won't let me do it-->
	</head>