<nav class="navbar-top" role="navigation">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
		<i class="fa fa-bars"></i> Menu
		</button>
		<div class="navbar-brand">
			<a href="index.html">
				<img src="img/tes2.png" data-1x="img/logo-starhub.png" style="margin-top: -8px; height: 40px; width: 135px; " data-2x="img/tes2b.png" class="hisrc img-responsive" alt="" >
			</a>
		</div>
	</div>
	<div class="nav-top">
		<ul class="nav navbar-left">
			<li class="tooltip-sidebar-toggle">
				<a href="#" id="sidebar-toggle" data-toggle="offcanvas" role="button" data-placement="right" title="Sidebar Toggle">
					<i class="fa fa-bars"></i>
				</a>
			</li>
		</ul>
	</div>
</nav>
<!-- begin SIDE NAVIGATION -->
<nav class="navbar-side" role="navigation">
	<div class="navbar-collapse sidebar-collapse collapse">
		<ul id="side" class="nav navbar-nav side-nav">
			<!-- begin SIDE NAV USER PANEL -->
			<li class="side-user hidden-xs" style="border-bottom: 0; top: 50px; margin-bottom: -10px;">
				<img src="img/logo contexio white-06.png" alt="" style="width: 80%;">
				<div class="clearfix"></div>
			</li>
			<li class="side-user ">
				<img class="img-circle hidden-xs" src="img/profile-pic2.jpg" alt="">
				<p class="welcome">
					Logged in as
				</p>
				<p class="name tooltip-sidebar-logout">
					"NAME" <a style="color: inherit" class="logout_open" href="#logout" data-toggle="tooltip" data-placement="top" title="Logout"><i class="fa fa-sign-out"></i></a>
				</p>
				<div class="clearfix"></div>
			</li>
			<li>
				<a class="active" href="index.php?dashboard">
					<span class="icon">
						<img src="img/images/9. dashboard icon.png" style="height: 10%; width: 15%; align-items: center;">
					</span> Executive Dashboard
				</a>
			</li>
			<li class="panel">
				<a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#ui-elements">
					<span class="icon" style="top: 8px;">
						<img src="img/acc.png" style="height: 10%; width: 15%; align-items: center;">
					</span> Accounts <i class="fa fa-caret-down"></i>
				</a>
				<ul class="collapse nav" id="ui-elements">
					<li>
						<a href="index.php?manage-accounts">
							<i class="fa fa-angle-double-right"></i> Account Management
						</a>
					</li>
					<li>
						<a href="index.php?page-policy">
							<i class="fa fa-angle-double-right"></i> Policy
						</a>
					</li>
					<li>
						<a href="index.php?page-event">
							<i class="fa fa-angle-double-right"></i> Events
						</a>
					</li>
					<li>
						<a href="#">
							<i class="fa fa-angle-double-right"></i> Communication Control
						</a>
					</li>
					<li>
						<a href="index.php?inventory">
							<i class="fa fa-angle-double-right"></i> Inventory
						</a>
					</li>
				</ul>
			</li>
			<li class="panel">
				<a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#ui-elementss">
					<span class="icon" style="top: 8px;">
						<img src="img/images/15. administration icon.png" style="height: 10%; width: 15%; align-items: center;">
					</span> Administrations <i class="fa fa-caret-down"></i>
				</a>
				<ul class="collapse nav" id="ui-elementss">
					<li>
						<a href="index.php?profile">
							<i class="fa fa-angle-double-right"></i> My Account
						</a>
					</li>
					<li>
						<a href="index.php?user-list">
							<i class="fa fa-angle-double-right"></i> My Users
						</a>
					</li>
				</ul>
			</li>
			<li>
				<a class="active" href="index.php?order-from">
					<span class="icon">
						<img src="img/images/9. dashboard icon.png" style="height: 10%; width: 15%; align-items: center;">
					</span> Order From
				</a>
			</li>
		</ul>
	</div>
</nav>