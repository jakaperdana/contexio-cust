<div id="page-wrapper">
	<div class="page-content">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>Executive Dashboard
					</h1>
					<ol class="breadcrumb">
						<li class="active">Executive Dashboard</li>
						<li class="active">Home</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-2">
						<div class="circle-tile">
							<a href="event-viewer.html">
								<div class="circle-tile-heading dark-blue">
									<i class="fa fa-globe fa-fw fa-3x"></i>
								</div>
							</a>
							<div class="circle-tile-content dark-blue">
								<div class="circle-tile-description text-faded" style="size: 14px;">
									WORKSTATION<br>LICENCE
								</div>
								<div class="circle-tile-number text-faded">
									265 <img src="img/1total.png" style="height: 10%; width: 20%; margin-bottom: -3px; margin-top: -15px;">
									<span id="sparklineA"></span>
								</div>
								<a href="events.html" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
							</div>
						</div>
					</div>
					<div class="col-lg-2">
						<div class="circle-tile">
							<a href="communication-control.html">
								<div class="circle-tile-heading blue">
									<i class="fa fa-desktop fa-fw fa-3x"></i>
								</div>
							</a>
							<div class="circle-tile-content blue">
								<div class="circle-tile-description text-faded">
									SERVER<br>LICENCE
								</div>
								<div class="circle-tile-number text-faded" style="margin-bottom: -0px;">
									32, 380 <i class="fa fa-server fa-fw fa-1x"></i>
								</div>
								<a href="communication-control.html" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
							</div>
						</div>
					</div>
					<div class="col-lg-2">
						<div class="circle-tile">
							<a href="inventory.html">
								<div class="circle-tile-heading green">
									<i class="fa fa-refresh fa-3x fa-fw"></i>
								</div>
							</a>
							<div class="circle-tile-content green">
								<div class="circle-tile-description text-faded">
									COLLECTOR<br> RUNNING
								</div>
								<div class="circle-tile-number text-faded">
									9  <img src="img/3pulse.png" style="height: 10%; width: 20%; margin-bottom: -5px;
									margin-top: -10px;">
								</div>
								<a href="inventory.html" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
							</div>
						</div>
					</div>
					<div class="col-lg-2">
						<div class="circle-tile">
							<a href="inventory.html">
								<div class="circle-tile-heading red">
									<i class="fa fa-ban fa-fw fa-3x"></i>
								</div>
							</a>
							<div class="circle-tile-content red">
								<div class="circle-tile-description text-faded">
									COLLECTOR<br> DISCONNECT
								</div>
								<div class="circle-tile-number text-faded">
									24 <i class="fa fa-power-off fa-fw fa-1x"></i>
									<span id="sparklineB"></span>
								</div>
								<a href="inventory.html" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
							</div>
						</div>
					</div>
					<div class="col-lg-2">
						<div class="circle-tile">
							<a href="inventory.html">
								<div class="circle-tile-heading orange">
									<i class="fa fa-exclamation-triangle fa-fw fa-3x"></i>
								</div>
							</a>
							<div class="circle-tile-content orange">
								<div class="circle-tile-description text-faded">
									COLLECTOR<br> DEGRADED
								</div>
								<div class="circle-tile-number text-faded">
									10 <img src="img/5degraded.png" style="height: 10%; width: 20%; margin-bottom: -5px;
									margin-top: -10px;">
									<span id="sparklineD"></span>
								</div>
								<a href="inventory.html" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
							</div>
						</div>
					</div>
					<div class="col-lg-2">
						<div class="circle-tile">
							<a href="inventory.html">
								<div class="circle-tile-heading gray">
									<i class="fa fa-times fa-fw fa-3x"></i>
								</div>
							</a>
							<div class="circle-tile-content gray">
								<div class="circle-tile-description text-faded">
									COLLECTOR<br>DISABLED
								</div>
								<div class="circle-tile-number text-faded">
									96 <img src="img/6disabel.png" style="height: 20px; width: 20px; margin-bottom: -5px;
									margin-top: -10px;">
									<span id="sparklineC"></span>
								</div>
								<a href="inventory.html" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end DASHBOARD CIRCLE TILES -->
		<div class="row">
			<div class="w100">
				<div class="col-lg-6">
					<div class="portlet portlet-green" style="border-color: #31485f;">
						<div class="portlet-heading" style="background-color: #31485f;">
							<div class="portlet-title">
								<h6>Events Destinations</h6>
							</div>
							<div class="clearfix"></div>
							<div class="portlet-body portlet-fix-gt">
								<div id="map"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="portlet portlet-default" style="border-color: #31485f;">
						<div class="portlet-heading" style="background-color: #31485f;">
							<div class="portlet-title">
								<h6>Latest Events</h6>
							</div>
							<div class="clearfix"></div>
							<div class="portlet-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover table-green">
										<thead>
											<tr>
												<th style="text-align: center;">DATE & TIME</th>
												<th style="text-align: center;">PROCESS NAME</th>
												<th style="text-align: center;">SEVERITY</th>
												<th style="text-align: center;">DEVICE</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td style="text-align: center;">26 - May - 2017 <br>11:26:38</td>
												<td style="text-align: center;">GooglePinyinDictionary.exe</td>
												<td style="text-align: left;"><i class="fa fa-circle" style="color:#ff0000"></i>&nbsp;
												CRITICAL</td>
												<td style="text-align: center;">BB115</td>
											</tr>
											<tr>
												<td style="text-align: center;">26 - May - 2017<br>11:25:46</td>
												<td style="text-align: center;">DbSynchronizer.exe</td>
												<td style="text-align: left;"><i class="fa fa-circle" style="color:#f39c12"></i>&nbsp;
												MEDIUM</td>
												<td style="text-align: center;">BB115</td>
											</tr>
											<tr>
												<td style="text-align: center;">26 - May - 2017<br>10:37:30</td>
												<td style="text-align: center;">DbSynchronizer.exe</td>
												<td style="text-align: left;"><i class="fa fa-circle" style="color:#f39c12"></i>&nbsp;
												MEDIUM</td>
												<td style="text-align: center;">HG682</td>
											</tr>
											<tr>
												<td style="text-align: center;">26 - May - 2017<br>9:51:39</td>
												<td style="text-align: center;">GooglePinyinDictionary.exe</td>
												<td style="text-align: left;"><i class="fa fa-circle" style="color:#ff0000"></i>&nbsp;
												CRITICAL</td>
												<td style="text-align: center;">HG682</td>
											</tr>
											<tr>
												<td style="text-align: center;">25 - May - 2017<br>10:20:03</td>
												<td style="text-align: center;">LMS.exe</td>
												<td style="text-align: left;"><i class="fa fa-circle" style="color:#f7941d"></i>&nbsp;
												HIGH</td>
												<td style="text-align: center;">SLS01</td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- /.table-responsive -->
							</div>
						</div>
					</div>
				</div>
				<div class="w100">
					<div class="col-lg-6">
						<div class="portlet portlet-green" style="border-color: #31485f;">
							<div class="portlet-heading" style="background-color: #31485f;">
								<div class="portlet-title">
									<h6>Users List</h6>
								</div>
								<div class="clearfix"></div>
								<div class="portlet-body2" style="padding: 10px; background-color: #fff; font-family: 'Lato', sans-serif; padding-bottom: 350px;">
									<div class="col-lg-3 col-md-3 col-sm-4 col-xs-4" style="color: #000;">
										<div class="user-member">
											<img class="img-circle" src="img/av1.png" alt="" data-pin-nopin="true">
											<h5>Alexander</h5>
										</div>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-4 col-xs-4" style="color: #000;">
										<div class="user-member">
											<img class="img-circle" src="img/av2.png" alt="" data-pin-nopin="true">
											<h5>Norman</h5>
										</div>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-4 col-xs-4" style="color: #000;">
										<div class="user-member">
											<img class="img-circle" src="img/av3.png" alt="" data-pin-nopin="true">
											<h5>Jane</h5>
										</div>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-4 col-xs-4" style="color: #000;">
										<div class="user-member">
											<img class="img-circle" src="img/av4.png" alt="" data-pin-nopin="true">
											<h5>John</h5>
										</div>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-4 col-xs-4" style="color: #000;">
										<div class="user-member">
											<img class="img-circle" src="img/ava1.png" alt="" data-pin-nopin="true">
											<h5>Alexander</h5>
										</div>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-4 col-xs-4" style="color: #000;">
										<div class="user-member">
											<img class="img-circle" src="img/ava2.png" alt="" data-pin-nopin="true">
											<h5>Sarah</h5>
										</div>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-4 col-xs-4" style="color: #000;">
										<div class="user-member">
											<img class="img-circle" src="img/ava3.png" alt="" data-pin-nopin="true">
											<h5>Nora</h5>
										</div>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-4 col-xs-4" style="color: #000;">
										<div class="user-member">
											<img class="img-circle" src="img/ava4.png" alt="" data-pin-nopin="true">
											<h5>Nadia</h5>
										</div>
									</div>
									<div class="col-lg-12" style="text-align: center; bottom: 10px; vertical-align: bottom;">
										<br>
										<a href="#" style="color: #3c8dbc;">View All Users</a>
										<br><br>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="portlet portlet-default" style="border-color: #31485f;">
							<div class="portlet-heading" style="background-color: #31485f;">
								<div class="portlet-title">
									<h6>Customer List</h6>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="portlet-body2" style="height: 360px;">
								<div  id="morris-chart-donut" style="display: none;"></div>
								<div class="col-lg-6">
									<img class="img-responsive center-block" src="img/partner3.png" alt="" style="width: 174px; height: 80px;">
								</div>
								<div class="col-lg-6">
									<img class="img-responsive center-block" src="img/partner2.jpg" alt="" style="width: 174px; height: 80px;">
								</div>
								<div class="col-lg-6">
									<img class="img-responsive center-block" src="img/partner4.jpg" alt="" style="width: 174px; height: 80px;">
								</div>
								<div class="col-lg-6">
									<img class="img-responsive center-block" src="img/partner1.png" alt="" style="width: 174px; height: 80px;">
								</div>
								<div class="col-lg-12" style="text-align: center; bottom: 15px;">
									<br>
									<a href="#">View All Users</a>
									<br><br>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="js/plugins/bootstrap/bootstrap.min.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
<script src="js/plugins/popupoverlay/defaults.js"></script>
<script src="js/plugins/popupoverlay/logout.js"></script>
<script src="js/plugins/hisrc/hisrc.js"></script>
<script src="js/plugins/messenger/messenger.min.js"></script>
<script src="js/plugins/messenger/messenger-theme-flat.js"></script>
<script src="js/plugins/daterangepicker/moment.js"></script>
<script src="js/plugins/daterangepicker/daterangepicker.js"></script>
<script src="js/plugins/flot/jquery.flot.js"></script>
<script src="js/plugins/flot/jquery.flot.resize.js"></script>
<script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="js/plugins/moment/moment.min.js"></script>
<script src="js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugins/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/demo/map-demo-data.js"></script>
<script src="js/plugins/easypiechart/jquery.easypiechart.min.js"></script>
<script src="js/plugins/datatables/jquery.dataTables.js"></script>
<script src="js/plugins/datatables/datatables-bs3.js"></script>
<script src="js/plugins/morris/raphael-2.1.0.min.js"></script>
<script src="js/plugins/morris/morris.js"></script>
<script src="js/flex.js"></script>
<script src="js/demo/dashboard-demo.js"></script>
<script src="plugins/jQuery/.jquery-2.2.3.min.js"></script>
<script src="js/plugins/bootstrap/bootstrap.min.js"></script>
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="js/app.min.js"></script>
<script src="js/demo.js"></script>
<script src="plugins/knob/jquery.knob.js"></script>
<!-- page script -->
<script>
jQuery(document).ready(function($) {
$(window).load(function(){
	$('#preloader').fadeOut('slow',function(){$(this).remove();});
});
});
$(function () {
/* jQueryKnob */
$(".knob").knob({
draw: function () {
	// "tron" case
	if (this.$.data('skin') == 'tron') {
		var a = this.angle(this.cv)
			, sa = this.startAngle
			, sat = this.startAngle
			, ea
			, eat = sat + a
			, r = true;
		this.g.lineWidth = this.lineWidth;
		this.o.cursor
			&& (sat = eat - 0.3)
			&& (eat = eat + 0.3);
		if (this.o.displayPrevious) {
			ea = this.startAngle + this.angle(this.value);
			this.o.cursor
				&& (sa = ea - 0.3)
				&& (ea = ea + 0.3);
			this.g.beginPath();
			this.g.strokeStyle = this.previousColor;
			this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
			this.g.stroke();
		}
		this.g.beginPath();
		this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
		this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
		this.g.stroke();
		this.g.lineWidth = 2;
		this.g.beginPath();
		this.g.strokeStyle = this.o.fgColor;
		this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
		this.g.stroke();
		return false;
	}
}
});
$(".sparkline").each(function () {
var $this = $(this);
$this.sparkline('html', $this.data());
});
drawDocSparklines();
drawMouseSpeedDemo();
});
function drawDocSparklines() {
$('#compositebar').sparkline('html', {type: 'bar', barColor: '#aaf'});
$('#compositebar').sparkline([4, 1, 5, 7, 9, 9, 8, 7, 6, 6, 4, 7, 8, 4, 3, 2, 2, 5, 6, 7],
	{composite: true, fillColor: false, lineColor: 'red'});
$('.sparkline-1').sparkline();
$('.largeline').sparkline('html',
	{type: 'line', height: '2.5em', width: '4em'});
$('#linecustom').sparkline('html',
	{
		height: '1.5em', width: '8em', lineColor: '#f00', fillColor: '#ffa',
		minSpotColor: false, maxSpotColor: false, spotColor: '#77f', spotRadius: 3
	});
$('.sparkbar').sparkline('html', {type: 'bar'});
$('.barformat').sparkline([1, 3, 5, 3, 8], {
type: 'bar',
tooltipFormat: '{{value:levels}} - {{value}}',
tooltipValueLookups: {
	levels: $.range_map({':2': 'Low', '3:6': 'Medium', '7:': 'High'})
}
});
$('.sparktristate').sparkline('html', {type: 'tristate'});
$('.sparktristatecols').sparkline('html',
	{type: 'tristate', colorMap: {'-2': '#fa7', '2': '#44f'}});
$('#compositeline').sparkline('html', {fillColor: false, changeRangeMin: 0, chartRangeMax: 10});
$('#compositeline').sparkline([4, 1, 5, 7, 9, 9, 8, 7, 6, 6, 4, 7, 8, 4, 3, 2, 2, 5, 6, 7],
	{composite: true, fillColor: false, lineColor: 'red', changeRangeMin: 0, chartRangeMax: 10});
$('#normalline').sparkline('html',
	{fillColor: false, normalRangeMin: -1, normalRangeMax: 8});
$('#normalExample').sparkline('html',
	{fillColor: false, normalRangeMin: 80, normalRangeMax: 95, normalRangeColor: '#4f4'});
$('.discrete1').sparkline('html',
	{type: 'discrete', lineColor: 'blue', xwidth: 18});
$('#discrete2').sparkline('html',
	{type: 'discrete', lineColor: 'blue', thresholdColor: 'red', thresholdValue: 4});
$('.sparkbullet').sparkline('html', {type: 'bullet'});
$('.sparkpie').sparkline('html', {type: 'pie', height: '1.0em'});
$('.sparkboxplot').sparkline('html', {type: 'box'});
$('.sparkboxplotraw').sparkline([1, 3, 5, 8, 10, 15, 18],
	{type: 'box', raw: true, showOutliers: true, target: 6});
$('.boxfieldorder').sparkline('html', {
type: 'box',
tooltipFormatFieldlist: ['med', 'lq', 'uq'],
tooltipFormatFieldlistKey: 'field'
});
$('.clickdemo').sparkline();
$('.clickdemo').bind('sparklineClick', function (ev) {
var sparkline = ev.sparklines[0],
region = sparkline.getCurrentRegionFields();
value = region.y;
alert("Clicked on x=" + region.x + " y=" + region.y);
});
$('.mouseoverdemo').sparkline();
$('.mouseoverdemo').bind('sparklineRegionChange', function (ev) {
var sparkline = ev.sparklines[0],
region = sparkline.getCurrentRegionFields();
value = region.y;
$('.mouseoverregion').text("x=" + region.x + " y=" + region.y);
}).bind('mouseleave', function () {
$('.mouseoverregion').text('');
});
}
function drawMouseSpeedDemo() {
var mrefreshinterval = 500;
var lastmousex = -1;
var lastmousey = -1;
var lastmousetime;
var mousetravel = 0;
var mpoints = [];
var mpoints_max = 30;
$('html').mousemove(function (e) {
var mousex = e.pageX;
var mousey = e.pageY;
if (lastmousex > -1) {
	mousetravel += Math.max(Math.abs(mousex - lastmousex), Math.abs(mousey - lastmousey));
}
lastmousex = mousex;
lastmousey = mousey;
});
var mdraw = function () {
var md = new Date();
var timenow = md.getTime();
if (lastmousetime && lastmousetime != timenow) {
	var pps = Math.round(mousetravel / (timenow - lastmousetime) * 1000);
	mpoints.push(pps);
	if (mpoints.length > mpoints_max)
		mpoints.splice(0, 1);
	mousetravel = 0;
	$('#mousespeed').sparkline(mpoints, {width: mpoints.length * 2, tooltipSuffix: ' pixels per second'});
}
lastmousetime = timenow;
setTimeout(mdraw, mrefreshinterval);
};
setTimeout(mdraw, mrefreshinterval);
}
</script>