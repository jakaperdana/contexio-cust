<div id="page-wrapper">
	<div class="page-content">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>
					Edit Account
					</h1>
					<ol class="breadcrumb">
						<li>
							<a href="index.html">Executive Dashboard /</a>
						</li>
						<a href="manage-accounts.html">Accounts Management /</a>
						<li class="active">Edit Account</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-body">
						<div class="row">
							<div class="col-sm-3">
								<br><br>
								<img class="img-responsive img-profile" src="img/profile-full2ab.jpg" alt="" style="max-width: 90%; margin-left: 15px; margin-top: 0px;">
								<a href="#">
									<span class="profile-edit" style="margin-left: 15px;margin-top: -40px;">Edit</span>
								</a>
							</div>
							<div class="col-sm-9">
								<div id="userSettingsContent" class="tab-content">
									<div class="tab-pane fade in active" id="basicInformation">
										<form role="form">
											<h4 class="page-header">Personal Information:</h4>
											<div class="form-group">
												<label>Nick Name</label>
												<input type="text" class="form-control" value="Handi">
											</div>
											<div class="form-group">
												<label>First Name</label>
												<input type="text" class="form-control" value="Handi">
											</div>
											<div class="form-group">
												<label>Middle Name</label>
												<input type="text" class="form-control" value="Setiyadi">
											</div>
											<div class="form-group">
												<label>Last Name</label>
												<input type="text" class="form-control" value="Setiyadi">
											</div>
											<div class="form-group">
												<label><i class="fa fa-envelope-o fa-fw"></i> Email </label>
												<input type="email" class="form-control" value="handi@arim-tech.com">
											</div>
											<div class="form-group">
												<label>Phone</label>
												<input type="tel" class="form-control" value="021-84308011">
											</div>
											<div class="form-group">
												<label><i class="fa fa-building-o fa-fw"></i> Role</label>
												<select multiple class="form-control">
													<option>Distributor</option>
													<option>Reseller</option>
													<option selected>Customer</option>
												</select>
											</div>
											<div class="form-group">
												<label><i class="fa fa-building-o fa-fw"></i> Departments</label>
												<input type="email" class="form-control" value="">
											</div>
											<button type="submit" class="btn btn-default">Save</button>
											<button class="btn btn-green">Cancel</button>
										</form>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="profile-settings">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="js/plugins/bootstrap/bootstrap.min.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
<script src="js/plugins/popupoverlay/defaults.js"></script>
<script src="js/plugins/popupoverlay/logout.js"></script>
<script src="js/plugins/hisrc/hisrc.js"></script>
<script src="js/flex.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
$(window).load(function(){
$('#preloader').fadeOut('slow',function(){$(this).remove();});
});
});
</script>