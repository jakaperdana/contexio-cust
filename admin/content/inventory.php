<div id="page-wrapper">
	<div class="page-content">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>
					Inventory Detail
					</h1>
					<ol class="breadcrumb">
						<li>
							<a href="index.html">Executive Dashboard /</a>
						</li>
						<li class="active">Inventory Detail</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="row">
			<div style="margin-top:20px;" class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-heading text-center">
						<div class="portlet-title">
							<h6>Inventory </h6>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="table-responsive table-arim">
						<div class="export-table">
							<div class="btn-group ta-action-left-acr">
								<button type="button" class="btn btn-default btn-arim dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-share-square-o"></i>
								Export
								<span class="caret"></span>
								</button>
								<ul class="dropdown-menu" style="text-align: left;">
									<li><a href="#">.CSV</a></li>
									<li><a href="#">.XML</a></li>
									<li><a href="#">.JSON</a></li>
								</ul>
								<div class="btn-group ta-action-left-acr" style="margin-top: -1px;width: 95%;margin-left: -260px;">
									<button  onClick='location.href="index.php?add-account"' type="button" class="btn btn-default" >
									<img src="img/acc.png" style="height: 20px; width: 20px; align-items: center;">Add Account
									</button>
								</div>
							</div>
						</div>
						<table id="datatable" class="table table-striped table-bordered table-hover table-blue" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th class="text-center">Name</th>
									<th class="text-center">Operating System</th>
									<th class="text-center">IP Address</th>
									<th class="text-center">State</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>iFast</td>
									<td class="text-center">Customer</td>
									<td onClick='location.href="index.php?detail-account"' class="text-center">
										192.1.1.1
									</td>
									<td style="text-align: left;"><i class="fa fa-circle" style="color:#ff0000"></i>&nbsp;
									CRITICAL</td>
								</tr>
								<tr>
									<td>iFast</td>
									<td class="text-center">Customer</td>
									<td onClick='location.href="index.php?detail-account"' class="text-center">
										192.1.1.1
									</td>
									<td style="text-align: left;"><i class="fa fa-circle" style="color:#ff0000"></i>&nbsp;
									CRITICAL</td>
								</tr>
								<tr>
									<td>iFast</td>
									<td class="text-center">Customer</td>
									<td onClick='location.href="index.php?detail-account"' class="text-center">
										192.1.1.1
									</td>
									<td style="text-align: left;"><i class="fa fa-circle" style="color:#ff0000"></i>&nbsp;
									CRITICAL</td>
								</tr>
								<tr>
									<td>iFast</td>
									<td class="text-center">Customer</td>
									<td onClick='location.href="index.php?detail-account"' class="text-center">
										192.1.1.1
									</td>
									<td style="text-align: left;"><i class="fa fa-circle" style="color:#ff0000"></i>&nbsp;
									CRITICAL</td>
								</tr>
								<tr>
									<td>iFast</td>
									<td class="text-center">Customer</td>
									<td onClick='location.href="index.php?detail-account"' class="text-center">
										192.1.1.1
									</td>
									<td style="text-align: left;"><i class="fa fa-circle" style="color:#ff0000"></i>&nbsp;
									CRITICAL</td>
								</tr>
								<tr>
									<td>iFast</td>
									<td class="text-center">Customer</td>
									<td onClick='location.href="index.php?detail-account"' class="text-center">
										192.1.1.1
									</td>
									<td style="text-align: left;"><i class="fa fa-circle" style="color:#ff0000"></i>&nbsp;
									CRITICAL</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="js/plugins/bootstrap/bootstrap.min.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
<script src="js/plugins/popupoverlay/defaults.js"></script>
<script src="js/plugins/popupoverlay/logout.js"></script>
<script src="js/plugins/hisrc/hisrc.js"></script>
<script src="js/flex.js"></script>
<script src="js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugins/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/demo/map-demo-data.js"></script>
<script src="js/plugins/easypiechart/jquery.easypiechart.min.js"></script>
<script src="js/plugins/datatables/jquery.dataTables.js"></script>
<script src="js/plugins/datatables/datatables-bs3.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
// site preloader -- also uncomment the div in the header and the css style for #preloader
$(window).load(function(){
$('#preloader').fadeOut('slow',function(){$(this).remove();});
});
});
$(document).ready(function() {
$('#datatable').dataTable({
"oLanguage" : {
"sSearch":"_INPUT_"
}
});
$('div.dataTables_filter input').attr('placeholder', 'Search...');
});
jQuery(document).ready(function($) {
// site preloader -- also uncomment the div in the header and the css style for #preloader
});
</script>