<div id="page-wrapper">
	<div class="page-content">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>
					Event Detail
					</h1>
					<ol class="breadcrumb">
						<li>
							<a href="index.html">Executive Dashboard /</a>
						</li>
						<li class="active">Event Detail</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="row">
			<div style="margin-top:20px;" class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-heading text-center">
						<div class="portlet-title">
							<h6>Event </h6>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="table-responsive table-arim">
						<div class="export-table">
							<div class="btn-group ta-action-left-acr">
								<button type="button" class="btn btn-default btn-arim dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-share-square-o"></i>
								Export
								<span class="caret"></span>
								</button>
								<ul class="dropdown-menu" style="text-align: left;">
									<li><a href="#">.CSV</a></li>
									<li><a href="#">.XML</a></li>
									<li><a href="#">.JSON</a></li>
								</ul>
								<div class="btn-group ta-action-left-acr" style="margin-top: -1px;width: 95%;margin-left: -260px;">
									<button  onClick='location.href="index.php?add-account"' type="button" class="btn btn-default" >
									<img src="img/acc.png" style="height: 20px; width: 20px; align-items: center;">Add Account
									</button>
								</div>
							</div>
						</div>
						<table id="datatable" class="table table-striped table-bordered table-hover table-blue" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th class="text-center">Device Name</th>
									<th class="text-center">Process Name</th>
									<th class="text-center">Last Update</th>
									<th class="text-center">Sevenity</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>iFast</td>
									<td class="text-center">Customer</td>
									<td onClick='location.href="index.php?detail-account"' class="text-center">
										<img src="img/application_view_detail.png" style="height: 20px; width: 20px; align-items: center;">
									</td>
									<td onClick='location.href="user-detail.html"' class="text-center">
										<span style="background: yellow; padding:5px 10px  ; border-radius:10px;">HIGH</span>
									</td>
								</tr>
								<tr>
									<td>iFast</td>
									<td class="text-center">Customer</td>
									<td onClick='location.href="index.php?detail-account"' class="text-center">
										<img src="img/application_view_detail.png" style="height: 20px; width: 20px; align-items: center;">
									</td>
									<td onClick='location.href="user-detail.html"' class="text-center">
										<span style="background: yellow; padding:5px 10px  ; border-radius:10px;">HIGH</span>
									</td>
								</tr>
								<tr>
									<td>iFast</td>
									<td class="text-center">Customer</td>
									<td onClick='location.href="index.php?detail-account"' class="text-center">
										<img src="img/application_view_detail.png" style="height: 20px; width: 20px; align-items: center;">
									</td>
									<td onClick='location.href="user-detail.html"' class="text-center">
										<span style="background: yellow; padding:5px 10px  ; border-radius:10px;">HIGH</span>
									</td>
								</tr>
								<tr>
									<td>iFast</td>
									<td class="text-center">Customer</td>
									<td onClick='location.href="index.php?detail-account"' class="text-center">
										<img src="img/application_view_detail.png" style="height: 20px; width: 20px; align-items: center;">
									</td>
									<td onClick='location.href="user-detail.html"' class="text-center">
										<span style="background: yellow; padding:5px 10px ; border-radius:10px;">HIGH</span>
									</td>
								</tr>
								<tr>
									<td>iFast</td>
									<td class="text-center">Customer</td>
									<td onClick='location.href="index.php?detail-account"' class="text-center">
										<img src="img/application_view_detail.png" style="height: 20px; width: 20px; align-items: center;">
									</td>
									<td onClick='location.href="user-detail.html"' class="text-center">
										<span style="background: yellow; padding:5px 10px  ; border-radius:10px;">HIGH</span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-body">
						<ul id="userTab" class="nav nav-tabs nav-jaka">
							<li class="active col-md-3">
								<a class="text-center" href="#overview" data-toggle="tab">EVENT DETAIL</a>
							</li>
							<li class="col-md-3">
								<a class="text-center" href="#profile-settings" data-toggle="tab">AFFECTED DEVICES</a>
							</li>
							<li class="col-md-3">
								<a class="text-center" href="#collector-group" data-toggle="tab">TRIGGERED POLICIES</a>
							</li>
							<li class="col-md-3">
								<a class="text-center" href="#add-collector" data-toggle="tab">FORENSICS</a>
							</li>
						</ul>
						<div id="userTabContent" class="tab-content">
							<div class="tab-pane fade in active" id="overview">
								<div class="row">
									<div class="col-lg-12 col-md-12" >
										<table class="table table-striped table-bordered table-hover table-blue" cellspacing="0" width="100%">
											<tbody>
												<tr onClick='location.href="#"'>
													<td class="text-left">Event ID </td>
													<td class="text-left">GENJER GANTENG</td>
												</tr>
												<tr onClick='location.href="#"'>
													<td class="text-left">Device </td>
													<td class="text-left">genjer.com</td>
												</tr>
												<tr onClick='location.href="#"'>
													<td class="text-left">Process</td>
													<td class="text-left">10 Collyer Quay #26-01<br> Ocean Financial Centre Building<br>Singapore 049315</td>
												</tr>
												<tr onClick='location.href="#"'>
													<td class="text-left">Sevenity</td>
													<td class="text-left">65-6557 2601</td>
												</tr>
												<tr onClick='location.href="#"'>
													<td class="text-left">Number Of Destination</td>
													<td class="text-left">PICIiFast</td>
												</tr>
												<tr onClick='location.href="#"'>
													<td class="text-left">Resolved</td>
													<td class="text-left">piciifast@ifast.com</td>
												</tr>
												<tr onClick='location.href="#"'>
													<td class="text-left">Last Updated</td>
													<td class="text-left">100</td>
												</tr>
												<tr onClick='location.href="#"'>
													<td class="text-left">Process Type</td>
													<td class="text-left">20</td>
												</tr>
												<tr onClick='location.href="#"'>
													<td class="text-left">Process Path</td>
													<td class="text-left">20</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="profile-settings">
								<div class="row">
									<div class="col-md-12">
										// ini section 2
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="add-collector">
								<div class="row">
									<div class="col-md-12">
										// ini section 3
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="collector-group">
								<div class="row">
									<div class="col-md-12">
										// ini section 4
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="js/plugins/bootstrap/bootstrap.min.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
<script src="js/plugins/popupoverlay/defaults.js"></script>
<script src="js/plugins/popupoverlay/logout.js"></script>
<script src="js/plugins/hisrc/hisrc.js"></script>
<script src="js/plugins/messenger/messenger.min.js"></script>
<script src="js/plugins/messenger/messenger-theme-flat.js"></script>
<script src="js/plugins/daterangepicker/moment.js"></script>
<script src="js/plugins/daterangepicker/daterangepicker.js"></script>
<script src="js/plugins/morris/raphael-2.1.0.min.js"></script>
<script src="js/plugins/morris/morris.js"></script>
<script src="js/plugins/morris/morris-demo-data.js"></script>
<script src="js/plugins/flot/jquery.flot.js"></script>
<script src="js/plugins/flot/jquery.flot.resize.js"></script>
<script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="js/plugins/moment/moment.min.js"></script>
<script src="js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugins/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/demo/map-demo-data.js"></script>
<script src="js/plugins/easypiechart/jquery.easypiechart.min.js"></script>
<script src="js/plugins/datatables/jquery.dataTables.js"></script>
<script src="js/plugins/datatables/datatables-bs3.js"></script>
<script src="js/flex.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
$(window).load(function(){
$('#preloader').fadeOut('slow',function(){$(this).remove();});
});
});
$(document).ready(function() {
$('#datatable').dataTable({
"oLanguage" : {
"sSearch":"_INPUT_"
}
});
$('div.dataTables_filter input').attr('placeholder', 'Search...');
});
jQuery(document).ready(function($) {
});
</script>