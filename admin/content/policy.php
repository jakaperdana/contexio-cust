<div id="page-wrapper">
	<div class="page-content">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>
					Policy
					</h1>
					<ol class="breadcrumb">
						<li>
							<a href="index.html">Executive Dashboard /</a>
						</li>
						<li class="active">Policy</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="row">
			<div style="margin-top:20px;" class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-heading text-center">
						<div class="portlet-title w100">
							<h6 class="text-center-jaka w100">Summary Of Account </h6>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="row">
						<div class="col-sm-3">
							<ul id="userSettings" class="nav nav-pills nav-stacked">
								<li class="active">
									<a href="#basicInformation" data-toggle="tab"><i class="fa fa-user fa-fw"></i> Execution Prevention</a>
								</li>
								<li>
									<a href="#profilePicture" data-toggle="tab"><i class="fa fa-picture-o fa-fw"></i> Exfiltration Prevention</a>
								</li>
								<li>
									<a href="#profilePicture" data-toggle="tab"><i class="fa fa-picture-o fa-fw"></i> Ronesomeware Prevention</a>
								</li>
							</ul>
						</div>
						<div class="col-sm-9">
							<div id="userSettingsContent" class="tab-content">
								<div class="tab-pane fade in active" id="basicInformation">
									<div class="col-md-12">
										<div class="panel panel-default">
											<table class="table table-fixed">
												<thead>
													<tr>
														<th class="col-xs-3">#</th>
														<th class="col-xs-3">FILE NAME</th>
														<th class="col-xs-3">ACTION</th>
														<th class="col-xs-3">STATE</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td class="col-xs-3">1</td>
														<td class="col-xs-3">File Apa Ajah</td>
														<td class="col-xs-3"><i class="fa fa-times-circle" aria-hidden="true"></i></td>
														<td class="col-xs-3"><i class="fa fa-check" aria-hidden="true"></i></td>
													</tr>
													<tr>
														<td class="col-xs-3">1</td>
														<td class="col-xs-3">File Apa Ajah</td>
														<td class="col-xs-3"><i class="fa fa-times-circle" aria-hidden="true"></i></td>
														<td class="col-xs-3"><i class="fa fa-check" aria-hidden="true"></i></td>
													</tr>
													<tr>
														<td class="col-xs-3">1</td>
														<td class="col-xs-3">File Apa Ajah</td>
														<td class="col-xs-3"><i class="fa fa-times-circle" aria-hidden="true"></i></td>
														<td class="col-xs-3"><i class="fa fa-check" aria-hidden="true"></i></td>
													</tr>
													<tr>
														<td class="col-xs-3">1</td>
														<td class="col-xs-3">File Apa Ajah</td>
														<td class="col-xs-3"><i class="fa fa-times-circle" aria-hidden="true"></i></td>
														<td class="col-xs-3"><i class="fa fa-check" aria-hidden="true"></i></td>
													</tr>
													<tr>
														<td class="col-xs-3">1</td>
														<td class="col-xs-3">File Apa Ajah</td>
														<td class="col-xs-3"><i class="fa fa-times-circle" aria-hidden="true"></i></td>
														<td class="col-xs-3"><i class="fa fa-check" aria-hidden="true"></i></td>
													</tr>
													<tr>
														<td class="col-xs-3">1</td>
														<td class="col-xs-3">File Apa Ajah</td>
														<td class="col-xs-3"><i class="fa fa-times-circle" aria-hidden="true"></i></td>
														<td class="col-xs-3"><i class="fa fa-check" aria-hidden="true"></i></td>
													</tr>
													<tr>
														<td class="col-xs-3">1</td>
														<td class="col-xs-3">File Apa Ajah</td>
														<td class="col-xs-3"><i class="fa fa-times-circle" aria-hidden="true"></i></td>
														<td class="col-xs-3"><i class="fa fa-check" aria-hidden="true"></i></td>
													</tr>
													<tr>
														<td class="col-xs-3">1</td>
														<td class="col-xs-3">File Apa Ajah</td>
														<td class="col-xs-3"><i class="fa fa-times-circle" aria-hidden="true"></i></td>
														<td class="col-xs-3"><i class="fa fa-check" aria-hidden="true"></i></td>
													</tr>
													<tr>
														<td class="col-xs-3">1</td>
														<td class="col-xs-3">File Apa Ajah</td>
														<td class="col-xs-3"><i class="fa fa-times-circle" aria-hidden="true"></i></td>
														<td class="col-xs-3"><i class="fa fa-check" aria-hidden="true"></i></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="profilePicture">
									//section 2
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div style="margin-top:20px;" class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-heading text-center">
						<div class="portlet-title w100">
							<h6 class="text-center w100">Rule Detail </h6>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<table class="table table-striped table-bordered table-hover table-blue" cellspacing="0" width="100%">
								<tbody>
									<tr onClick='location.href="#"'>
										<th class="text-left">RULE NAME </th>
									</tr>
									<tr onClick='location.href="#"'>
										<td class="text-left">Lorem Ipsum Blablabl dasdasd dasdas </td>
									</tr>
									<tr onClick='location.href="#"'>
										<th class="text-left">RULE DETAILS </th>
									</tr>
									<tr onClick='location.href="#"'>
										<td class="text-left">Lorem Ipsum Blablabl dasdasd dasdas </td>
									</tr>
									<tr onClick='location.href="#"'>
										<th class="text-left">FORENSICS RECOMENDATIONS</th>
									</tr>
									<tr onClick='location.href="#"'>
										<td class="text-left">Lorem Ipsum Blablabl dasdasd dasdas Lorem Ipsum Blablabl dasdasd dasdas Lorem Ipsum Blablabl dasdasd dasdas Lorem Ipsum Blablabl dasdasd dasdas</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="js/plugins/bootstrap/bootstrap.min.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
<script src="js/plugins/popupoverlay/defaults.js"></script>
<script src="js/plugins/popupoverlay/logout.js"></script>
<script src="js/plugins/hisrc/hisrc.js"></script>
<script src="js/plugins/messenger/messenger.min.js"></script>
<script src="js/plugins/messenger/messenger-theme-flat.js"></script>
<script src="js/plugins/daterangepicker/moment.js"></script>
<script src="js/plugins/daterangepicker/daterangepicker.js"></script>
<script src="js/plugins/morris/raphael-2.1.0.min.js"></script>
<script src="js/plugins/morris/morris.js"></script>
<script src="js/plugins/morris/morris-demo-data.js"></script>
<script src="js/plugins/flot/jquery.flot.js"></script>
<script src="js/plugins/flot/jquery.flot.resize.js"></script>
<script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="js/plugins/moment/moment.min.js"></script>
<script src="js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugins/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/demo/map-demo-data.js"></script>
<script src="js/plugins/easypiechart/jquery.easypiechart.min.js"></script>
<script src="js/plugins/datatables/jquery.dataTables.js"></script>
<script src="js/plugins/datatables/datatables-bs3.js"></script>
<script src="js/flex.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
$(window).load(function(){
$('#preloader').fadeOut('slow',function(){$(this).remove();});
});
});
$(document).ready(function() {
$('#datatable').dataTable({
"oLanguage" : {
"sSearch":"_INPUT_"
}
});
$('div.dataTables_filter input').attr('placeholder', 'Search...');
});
jQuery(document).ready(function($) {
});
</script>