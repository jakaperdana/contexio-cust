<div id="page-wrapper">
	<div class="page-content">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<div class="page-title">
						<h1>User List
						</h1>
						<ol class="breadcrumb">
							<li>
								<a href="index.html">Executive Dashboard /</a>
							</li>
							<li class="active">User List</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-heading text-center">
						<div class="portlet-title w100">
							<h6 class="w100">Summary Of Users</h6>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="table-responsive table-arim">
						<div class="export-table">
							<div class="btn-group ta-action-left-acr">
								<button type="button" class="btn btn-default btn-arim dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-share-square-o"></i>
								Export
								<span class="caret"></span>
								</button>
								<ul class="dropdown-menu" style="text-align: left;">
									<li><a href="#">.CSV</a></li>
									<li><a href="#">.XML</a></li>
									<li><a href="#">.JSON</a></li>
								</ul>
								<div class="btn-group ta-action-left-acr" style="margin-top: -1px;width: 95%;margin-left: -260px;">
									<button  onClick='location.href="index.php?add-account"' type="button" class="btn btn-default" >
									<img src="img/acc.png" style="height: 20px; width: 20px; align-items: center;">Add Account
									</button>
								</div>
							</div>
						</div>
						<table id="datatable" class="table table-striped table-bordered table-hover table-blue" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th class="text-center">Name</th>
									<th class="text-center">Email</th>
									<th class="text-center">Role</th>
									<th class="text-center">Last Login</th>
									<th class="text-center">Status</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								<tr onClick='location.href="index.php?detail-account"'>
									<td>Person1</td>
									<td class="text-center">person1@company1.com</td>
									<td class="text-center">Administrator</td>
									<td class="text-center">2017-05-25 11:20:36</td>
									<td class="text-center">Active</td>
									<td class="text-center"><a href="index.php?event" class="btn btn-sm btn-default">Detail</a></td>
								</tr>
								<tr onClick='location.href="index.php?detail-account"'>
									<td>Person1</td>
									<td class="text-center">person1@company1.com</td>
									<td class="text-center">Administrator</td>
									<td class="text-center">2017-05-25 11:20:36</td>
									<td class="text-center">Active</td>
									<td class="text-center"><a href="index.php?event" class="btn btn-sm btn-default">Detail</a></td>
								</tr>
								<tr onClick='location.href="index.php?detail-account"'>
									<td>Person1</td>
									<td class="text-center">person1@company1.com</td>
									<td class="text-center">Administrator</td>
									<td class="text-center">2017-05-25 11:20:36</td>
									<td class="text-center">Active</td>
									<td class="text-center"><a href="index.php?event" class="btn btn-sm btn-default">Detail</a></td>
								</tr>
								<tr onClick='location.href="index.php?detail-account"'>
									<td>Person1</td>
									<td class="text-center">person1@company1.com</td>
									<td class="text-center">Administrator</td>
									<td class="text-center">2017-05-25 11:20:36</td>
									<td class="text-center">Active</td>
									<td class="text-center"><a href="index.php?event" class="btn btn-sm btn-default">Detail</a></td>
								</tr>
								<tr onClick='location.href="index.php?detail-account"'>
									<td>Person1</td>
									<td class="text-center">person1@company1.com</td>
									<td class="text-center">Administrator</td>
									<td class="text-center">2017-05-25 11:20:36</td>
									<td class="text-center">Active</td>
									<td class="text-center"><a href="index.php?event" class="btn btn-sm btn-default">Detail</a></td>
								</tr>
								<tr onClick='location.href="index.php?detail-account"'>
									<td>Person1</td>
									<td class="text-center">person1@company1.com</td>
									<td class="text-center">Administrator</td>
									<td class="text-center">2017-05-25 11:20:36</td>
									<td class="text-center">Active</td>
									<td class="text-center"><a href="index.php?event" class="btn btn-sm btn-default">Detail</a></td>
								</tr>
								<tr onClick='location.href="index.php?detail-account"'>
									<td>Person1</td>
									<td class="text-center">person1@company1.com</td>
									<td class="text-center">Administrator</td>
									<td class="text-center">2017-05-25 11:20:36</td>
									<td class="text-center">Active</td>
									<td class="text-center"><a href="index.php?event" class="btn btn-sm btn-default">Detail</a></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="js/plugins/bootstrap/bootstrap.min.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
<script src="js/plugins/popupoverlay/defaults.js"></script>
<script src="js/plugins/popupoverlay/logout.js"></script>
<script src="js/plugins/hisrc/hisrc.js"></script>
<script src="js/plugins/messenger/messenger.min.js"></script>
<script src="js/plugins/messenger/messenger-theme-flat.js"></script>
<script src="js/plugins/daterangepicker/moment.js"></script>
<script src="js/plugins/daterangepicker/daterangepicker.js"></script>
<script src="js/plugins/morris/raphael-2.1.0.min.js"></script>
<script src="js/plugins/morris/morris.js"></script>
<script src="js/plugins/morris/morris-demo-data.js"></script>
<script src="js/plugins/flot/jquery.flot.js"></script>
<script src="js/plugins/flot/jquery.flot.resize.js"></script>
<script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="js/plugins/moment/moment.min.js"></script>
<script src="js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugins/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/demo/map-demo-data.js"></script>
<script src="js/plugins/easypiechart/jquery.easypiechart.min.js"></script>
<script src="js/plugins/datatables/jquery.dataTables.js"></script>
<script src="js/plugins/datatables/datatables-bs3.js"></script>
<script src="js/flex.js"></script>
<script type="text/javascript">
$(document).ready(function() {
$('#datatable').dataTable({
"oLanguage" : {
"sSearch":"_INPUT_"
}
});
$('div.dataTables_filter input').attr('placeholder', 'Search...');
});
jQuery(document).ready(function($) {
$(window).load(function(){
$('#preloader').fadeOut('slow',function(){$(this).remove();});
});
});
</script>