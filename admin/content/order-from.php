<div id="page-wrapper">
	<div class="page-content">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>
					Order From
					</h1>
					<ol class="breadcrumb">
						<li>
							<a href="index.html">Executive Dashboard /</a>
						</li>
						<a href="manage-accounts.html">Accounts Management /</a>
						<li class="active">Order From</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-body">
						<div class="row">
							<div class="col-sm-12">
								<div id="userSettingsContent" class="tab-content">
									<div class="tab-pane fade in active" id="basicInformation">
										<form role="form">
											<div class="form-group">
												<label>Name</label>
												<input type="text" class="form-control" >
											</div>
											<div class="form-group">
												<label>Type</label>
												<select class="form-control selectpicker" data-hide-disabled="true" data-show-subtext="true" data-live-search="true">
													<option value="" disabled selected></option>
													<option value="update" data-subtext="old subtext">Update subtext</option>
													<option value="delete" data-subtext="more subtext">Delete subtext</option>
												</select>
											</div>
											<div class="form-group">
												<div class='row'>
													<div class='col-sm-6'>
														<div class='form-group'>
															<label for="user_firstname">Server License</label>
															<input class="form-control" id="user_firstname" name="user[firstname]" required="true" size="30" type="text" />
														</div>
													</div>
													<div class='col-sm-6'>
														<div class='form-group'>
															<label for="user_firstname">Workstation License</label>
															<input class="form-control" id="user_firstname" name="user[firstname]" required="true" size="30" type="text" />
														</div>
													</div>
													<div class='col-sm-6'>
														<div class='form-group'>
															<label for="user_firstname">Starting Date</label>
															<input class="form-control" id="user_firstname" name="user[firstname]" required="true" size="30" type="text" />
														</div>
													</div>
													<div class='col-sm-6'>
														<div class='form-group'>
															<label for="user_firstname">End Date</label>
															<input class="form-control" id="user_firstname" name="user[firstname]" required="true" size="30" type="text" />
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label>Address</label>
												<input type="text" class="form-control" >
											</div>
											<div class="form-group">
												<label>Phone</label>
												<input type="text" class="form-control" >
											</div>
											<div class="form-group">
												<label>PIC</label>
												<input type="tel" class="form-control">
											</div>
											<div class="form-group">
												<label><i class="fa fa-envelope-o fa-fw"></i> PIC Phone Number </label>
												<input type="text" class="form-control" >
											</div>
											<div class="form-group">
												<label><i class="fa fa-envelope-o fa-fw"></i> PIC Email </label>
												<input type="email" class="form-control" >
											</div>
											<button type="submit" class="btn btn-default">Save</button>
											<button class="btn btn-green">Cancel</button>
										</form>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="profile-settings">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="js/plugins/bootstrap/bootstrap.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
<script src="js/plugins/popupoverlay/defaults.js"></script>
<script src="js/plugins/popupoverlay/logout.js"></script>
<script src="js/plugins/hisrc/hisrc.js"></script>
<script src="js/flex.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
	$(".selectpicker").selectpicker();
$(window).load(function(){
$('#preloader').fadeOut('slow',function(){$(this).remove();});
});
});
</script>