<div id="page-wrapper">
	<div class="page-content">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<div class="page-title">
						<h1>Accounts Management
						</h1>
						<ol class="breadcrumb">
							<li>
								<a href="index.html">Executive Dashboard /</a>
							</li>
							<li>
								<a href="manage-accounts.html" >Accounts Summary /</a>
							</li>
							<li class="active">Account Details</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-body">
						<ul id="userTab" class="nav nav-tabs">
							<li class="active">
								<a href="#overview" data-toggle="tab">Overview</a>
							</li>
							<li>
								<a href="#profile-settings" data-toggle="tab">Update Account</a>
							</li>
							<li>
								<a href="#collector-group" data-toggle="tab">Collector Group</a>
							</li>
							<li>
								<a href="#add-collector" data-toggle="tab">Add Collector Group</a>
							</li>
						</ul>
						<div id="userTabContent" class="tab-content">
							<div class="tab-pane fade in active" id="overview">
								<div class="row">
									<div class="col-lg-4 col-md-4">
										<br><br>
										<img class="img-responsive img-profile" src="img/profile-full2ab.jpg" alt="" style="max-width: 100%; ">
									</div>
									<div class="col-lg-8 col-md-8" >
										<table class="table table-striped table-bordered table-hover table-blue" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th colspan="2" class="text-center">PROFILE</th>
												</tr>
											</thead>
											<tbody>
												<tr onClick='location.href="account-details.html"'>
													<td class="text-left">Name </td>
													<td class="text-left">iFAST</td>
												</tr>
												<tr onClick='location.href="account-details.html"'>
													<td class="text-left">Type </td>
													<td class="text-left">Customer</td>
												</tr>
												<tr onClick='location.href="account-details.html"'>
													<td class="text-left">Address</td>
													<td class="text-left">10 Collyer Quay #26-01<br> Ocean Financial Centre Building<br>Singapore 049315</td>
												</tr>
												<tr onClick='location.href="account-details.html"'>
													<td class="text-left">Phone</td>
													<td class="text-left">65-6557 2601</td>
												</tr>
												<tr onClick='location.href="account-details.html"'>
													<td class="text-left">PIC</td>
													<td class="text-left">PICIiFast</td>
												</tr>
												<tr onClick='location.href="account-details.html"'>
													<td class="text-left">PIC Email</td>
													<td class="text-left">piciifast@ifast.com</td>
												</tr>
												<tr onClick='location.href="account-details.html"'>
													<td class="text-left">Workstation License</td>
													<td class="text-left">100</td>
												</tr>
												<tr onClick='location.href="account-details.html"'>
													<td class="text-left">Server License</td>
													<td class="text-left">20</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="profile-settings">
								<div class="row">
									<div class="col-sm-3">
										<ul id="userSettings" class="nav nav-pills nav-stacked">
											<li class="active">
												<a href="#basicInformation" data-toggle="tab"><i class="fa fa-user fa-fw"></i> Basic Information</a>
											</li>
											<li>
												<a href="#profilePicture" data-toggle="tab"><i class="fa fa-picture-o fa-fw"></i> Account Logo</a>
											</li>
										</ul>
									</div>
									<div class="col-sm-9">
										<div id="userSettingsContent" class="tab-content">
											<div class="tab-pane fade in active" id="basicInformation">
												<form role="form">
													<h4 class="page-header">Personal Information:</h4>
													<div class="form-group">
														<label>First Name</label>
														<input type="text" class="form-control" value="iFAST">
													</div>
													<div class="form-group">
														<label>Type</label>
														<select multiple class="form-control">
															<option>Distributor</option>
															<option>Reseller</option>
															<option >Customer</option>
														</select>
													</div>
													<div class="form-group">
														<label>Address</label>
														<input type="text" class="form-control" value="10 Collyer Quay #26-01<br> Ocean Financial Centre Building<br>Singapore 049315">
													</div>
													<div class="form-group">
														<label>Phone</label>
														<input type="tel" class="form-control" value="65-6557 2601">
													</div>
													<div class="form-group">
														<label>PIC</label>
														<input type="tel" class="form-control" value="PICIiFast">
													</div>
													<div class="form-group">
														<label>PIC Email</label>
														<input type="email" class="form-control" value="piciifast@ifast.com">
													</div>
													<button type="submit" class="btn btn-default">Update Profile</button>
													<button class="btn btn-green">Cancel</button>
												</form>
											</div>
											<div class="tab-pane fade" id="profilePicture">
												<h3>Current Picture:</h3>
												<img class="img-responsive img-profile" src="img/profile-full2ab.jpg" alt="">
												<br>
												<form role="form">
													<div class="form-group">
														<label>Choose a New Image</label>
														<input type="file">
														<p class="help-block"><i class="fa fa-warning"></i> Image must be no larger than 500x500 pixels. Supported formats: JPG, GIF, PNG</p>
														<button type="submit" class="btn btn-default">Update Account Logo</button>
														<button class="btn btn-green">Cancel</button>
													</div>
												</form>
											</div> <br><br>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="add-collector">
								<div class="row">
									<div class="col-sm-9">
										<div id="userSettingsContent" class="tab-content">
											<div class="tab-pane fade in active" id="basicInformation">
												<form role="form">
													<div class="form-group">
														<label>Collector Group Name</label>
														<input type="text" class="form-control" value="">
													</div>
													<div class="form-group">
														<label>Number Of Endpoint</label>
														<input type="text" class="form-control" value="">
													</div>
													<button type="submit" class="btn btn-default">Save</button>
													<button class="btn btn-green">Cancel</button>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="collector-group">
								<div class="portlet portlet-default">
									<div class="table-responsive table-arim" style=" padding: 10px;width: 100%;align-content: center; margin-left: 0;">
										<table id="datatable" class="table table-striped table-bordered table-hover table-blue" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th class="text-center">Collector Group Name</th>
													<th class="text-center">Number Of Endpoint</th>
												</tr>
											</thead>
											<tbody>
												<tr onClick='location.href="account-details.html"'>
													<td class="text-center">iFast-CG1</td>
													<td class="text-center">30</td>
												</tr>
												<tr onClick='location.href="account-details.html"'>
													<td class="text-center">iFast-CG2</td>
													<td class="text-center">10</td>
												</tr>
												<tr onClick='location.href="account-details.html"'>
													<td class="text-center">iFast-CG3</td>
													<td class="text-center">50</td>
												</tr>
												<tr onClick='location.href="account-details.html"'>
													<td class="text-center">iFast-CG3</td>
													<td class="text-center">50</td>
												</tr>
												<tr onClick='location.href="account-details.html"'>
													<td class="text-center">iFast-CG3</td>
													<td class="text-center">50</td>
												</tr>
												<tr onClick='location.href="account-details.html"'>
													<td class="text-center">iFast-CG3</td>
													<td class="text-center">50</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="js/plugins/bootstrap/bootstrap.min.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
<script src="js/plugins/popupoverlay/defaults.js"></script>
<script src="js/plugins/popupoverlay/logout.js"></script>
<script src="js/plugins/hisrc/hisrc.js"></script>
<script src="js/plugins/messenger/messenger.min.js"></script>
<script src="js/plugins/messenger/messenger-theme-flat.js"></script>
<script src="js/plugins/daterangepicker/moment.js"></script>
<script src="js/plugins/daterangepicker/daterangepicker.js"></script>
<script src="js/plugins/morris/raphael-2.1.0.min.js"></script>
<script src="js/plugins/morris/morris.js"></script>
<script src="js/plugins/morris/morris-demo-data.js"></script>
<script src="js/plugins/flot/jquery.flot.js"></script>
<script src="js/plugins/flot/jquery.flot.resize.js"></script>
<script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="js/plugins/moment/moment.min.js"></script>
<script src="js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/plugins/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/demo/map-demo-data.js"></script>
<script src="js/plugins/easypiechart/jquery.easypiechart.min.js"></script>
<script src="js/plugins/datatables/jquery.dataTables.js"></script>
<script src="js/plugins/datatables/datatables-bs3.js"></script>
<script src="js/flex.js"></script>
<script type="text/javascript">
function myFunction() {
var input, filter, table, tr, td, i;
input = document.getElementById("myInput");
filter = input.value.toUpperCase();
table = document.getElementById("myTable");
tr = table.getElementsByTagName("tr");
for (i = 0; i < tr.length; i++) {
td = tr[i].getElementsByTagName("td")[0];
if (td) {
if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
tr[i].style.display = "";
} else {
tr[i].style.display = "none";
}
}
}
}
$(document).ready(function() {
$('#datatable').dataTable({
"oLanguage" : {
"sSearch":"_INPUT_"
}
});
$('div.dataTables_filter input').attr('placeholder', 'Search...');
});
jQuery(document).ready(function($) {
$(window).load(function(){
$('#preloader').fadeOut('slow',function(){$(this).remove();});
});
});
</script>